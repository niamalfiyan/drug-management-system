<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugInsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_ins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('drug_receive_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->double('qty_in',15,8)->default(0);
            $table->double('stock',15,8)->default(0);
            $table->double('reserved',15,8)->default(0);
            $table->string('note')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('drug_receive_id')->references('id')->on('drug_receives')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drug_ins');
    }
}
