<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryStockView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
      CREATE VIEW summary_stock AS
        select drugs.id,
        drugs.code,
        drugs.name,
        sum(drug_receives.stock) as stock
        from drug_receives
        join drugs on drug_receives.drug_id = drugs.id
        where stock > 0
        GROUP BY drugs.id,
        drugs.code,
        drugs.name
        union all
        select drugs.id,
        drugs.code,
        drugs.name,
        sum(drug_ins.stock) as stock
        from drug_receives
        join drugs on drug_receives.drug_id = drugs.id
        join drug_ins on drug_receives.id = drug_ins.drug_receive_id
        where drug_ins.stock > 0
        GROUP BY drugs.id,
        drugs.code,
        drugs.name

    ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS summary_stock');
    }
}
