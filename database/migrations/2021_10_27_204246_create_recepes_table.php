<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecepesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pasien');
            $table->string('alamat');
            $table->string('jenis_kelamin');
            $table->string('diagnosis');
            $table->string('berat_badan');
            $table->string('tekanan_darah');
            $table->string('status_jaminan');
            $table->string('tempat_pemeriksaan');
            $table->timestamp('tanggal_resep')->nullable();
            $table->timestamp('tanggal_lahir')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('doctor_id')->unsigned();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('doctor_id')->references('id')->on('doctors')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
