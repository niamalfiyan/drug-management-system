<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportOutView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
      CREATE VIEW report_out AS
      select drugs.code,
      drugs.`name`,
      drugs.type,
      drug_receives.satuan,
      date(drug_receives.expired_date) as expired_date,
      transactions.qty,
      transactions.created_at,
      users.name as username
      from drug_receives 
      join transactions on drug_receives.id = transactions.drug_receive_id
      left join drugs on drug_receives.drug_id = drugs.id
      join users on transactions.user_id = users.id
      where transactions.`status` ='out'      

    ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS report_out');
    }
}
