<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportReceiveView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
      CREATE VIEW report_receive AS
      select drugs.code,
    drugs.`name`,
    drugs.type,
    drug_receives.satuan,
    drug_receives.expired_date,
    drug_receives.qty_receive,
    drug_receives.created_at,
    users.name as username,
    drug_receives.note
    from drug_receives
    left join drugs on drug_receives.drug_id = drugs.id
    left join users on drug_receives.user_id = users.id
      

    ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS report_receive');
    }
}
