const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .scripts([
        'resources/js/mustache.js',
        'resources/js/limitless/libraries/jquery.min.js',
        'resources/js/limitless/libraries/bootstrap.bundle.min.js',
        'resources/js/limitless/libraries/blockui.min.js',
        'resources/js/limitless/libraries/ripple.min.js',
        'resources/js/limitless/libraries/uniform.min.js',
        'resources/js/plugins/forms/selects/select2.min.js',
        'resources/js/plugins/tables/datatables/datatables.min.js',
        'resources/js/plugins/tables/datatables/extensions/responsive.min.js',
        'resources/js/plugins/tables/datatables/extensions/fixed_columns.min.js',
        'resources/js/pages/layout_fixed_native.js',
        'resources/js/limitless/libraries/sweet_alert.min.js',
        'resources/js/limitless/app.js',
    ], 'public/js/limitless.js')
    .scripts([
        'resources/js/plugins/forms/tags/tagsinput.min.js',
        'resources/js/plugins/forms/tags/tokenfield.min.js',
        'resources/js/plugins/ui/prism.min.js',
        'resources/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',
        'resources/js/pages/form_tags_input.js'
    ], 'public/js/tags.js')
    .scripts([
        'resources/js/plugins/uploaders/fileinput/plugins/purify.min.js',
        'resources/js/plugins/uploaders/fileinput/plugins/sortable.min.js',
        'resources/js/plugins/uploaders/fileinput/fileinput.min.js',
    ], 'public/js/fileinput.js')
    .scripts([
        'resources/js/plugins/pickers/pickadate/picker.js',
        'resources/js/plugins/pickers/pickadate/picker.date.js',
        'resources/js/pages/picker_date.js',
    ], 'public/js/datepicker.js')
    .scripts([
        'resources/js/plugins/ui/fab.min.js',
        'resources/js/pages/extra_fab.js'
    ], 'public/js/floating_button.js')
    .scripts([
        'resources/js/plugins/forms/styling/uniform.min.js',
        'resources/js/plugins/forms/styling/switchery.min.js',
        'resources/js/plugins/forms/styling/switch.min.js',
        'resources/js/pages/form_checkboxes_radios.js',
    ], 'public/js/switch.js')
    .scripts([
        'resources/js/plugins/highchart/highcharts.js',
        'resources/js/plugins/highchart/exporting.js',
        'resources/js/plugins/highchart/export-data.js',
        'resources/js/plugins/highchart/series-label.js',
    ], 'public/js/highchart.js')
    .scripts([
        'resources/js/plugins/visualization/echarts/echarts.js'
    ], 'public/js/echarts.js')
    .scripts([
        "resources/js/plugins/notifications/bootbox.min.js",
        "resources/js/pages/notification.js",
    ], "public/js/notification.js")
    .scripts([
        'resources/js/pages/role.js',
    ], 'public/js/role.js')
    .scripts([
        'resources/js/pages/permission.js',
    ], 'public/js/permission.js')
    .scripts([
        'resources/js/pages/user.js',
    ], 'public/js/user.js')
    .scripts([
        'resources/js/pages/drug.js',
    ], 'public/js/drug.js')
    .scripts([
        'resources/js/pages/doctor.js',
    ], 'public/js/doctor.js')
    .scripts([
        'resources/js/pages/drug_receive.js',
    ], 'public/js/drug_receive.js')
    .scripts([
        'resources/js/pages/drug_out.js',
    ], 'public/js/drug_out.js')
    .scripts([
        'resources/js/pages/drug_in.js',
    ], 'public/js/drug_in.js')
    .scripts([
        'resources/js/pages/report_lplpo.js',
    ], 'public/js/report_lplpo.js')
    .scripts([
        'resources/js/pages/report_receive.js',
    ], 'public/js/report_receive.js')
    .scripts([
        'resources/js/pages/report_out.js',
    ], 'public/js/report_out.js')
    .scripts([
        'resources/js/pages/report_distribution.js',
    ], 'public/js/report_distribution.js')
    .styles([
        'resources/css/limitless/bootstrap.min.css',
        'resources/css/limitless/bootstrap_limitless.min.css',
        'resources/css/limitless/layout.min.css',
        'resources/css/limitless/components.min.css',
        'resources/css/limitless/colors.min.css',
        'resources/css/limitless/custom.css'
    ], 'public/css/limitless.css')
    .copyDirectory('resources/images', 'public/images')
    .copyDirectory('resources/css/icons', 'public/css/icons')
    .sass('resources/sass/app.scss', 'public/css')
    .version();