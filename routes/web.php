<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');

Route::prefix('account-management')->group(function () 
{
    Route::prefix('permission')->middleware(['permission:menu-permission'])->group(function(){
        Route::get('', 'PermissionController@index')->name('permission.index');
        Route::get('create', 'PermissionController@create')->name('permission.create');
        Route::get('data', 'PermissionController@data')->name('permission.data');
        Route::post('store', 'PermissionController@store')->name('permission.store');
        Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
        Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
        Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
    });

    Route::prefix('role')->middleware(['permission:menu-role'])->group(function(){
        Route::get('', 'RoleController@index')->name('role.index');
        Route::get('create', 'RoleController@create')->name('role.create');
        Route::get('data', 'RoleController@data')->name('role.data');
        Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
        Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
        Route::post('store', 'RoleController@store')->name('role.store');
        Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
        Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
        Route::post('update/{id}', 'RoleController@update')->name('role.update');
        Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
    });

    Route::prefix('user')->middleware(['permission:menu-user'])->group(function(){
        Route::get('', 'UserController@index')->name('user.index');
        Route::get('create', 'UserController@create')->name('user.create');
        Route::get('data', 'UserController@data')->name('user.data');
        Route::get('get-absence', 'UserController@getAbsence')->name('user.getAbsence');
        Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
        Route::get('edit/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
        Route::post('store', 'UserController@store')->name('user.store');
        Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
        Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
        Route::post('update/{id}', 'UserController@update')->name('user.update');
        Route::put('reset-password/{id}', 'UserController@resetPassword')->name('user.resetPassword');
        Route::delete('delete/{id}', 'UserController@destroy')->name('user.destroy');
    });
});

Route::prefix('master-data')->group(function () 
{
    Route::prefix('drug')->middleware(['permission:menu-drug'])->group(function(){
        Route::get('', 'DrugController@index')->name('drug.index');
        Route::get('create', 'DrugController@create')->name('drug.create');
        Route::get('data', 'DrugController@data')->name('drug.data');
        Route::post('store', 'DrugController@store')->name('drug.store');
        Route::get('edit/{id}', 'DrugController@edit')->name('drug.edit');
        Route::post('update/{id}', 'DrugController@update')->name('drug.update');
        Route::delete('delete/{id}', 'DrugController@destroy')->name('drug.destroy');
    });

    Route::prefix('doctor')->middleware(['permission:menu-doctor'])->group(function(){
        Route::get('', 'DoctorController@index')->name('doctor.index');
        Route::get('create', 'DoctorController@create')->name('doctor.create');
        Route::get('data', 'DoctorController@data')->name('doctor.data');
        Route::post('store', 'DoctorController@store')->name('doctor.store');
        Route::get('edit/{id}', 'DoctorController@edit')->name('doctor.edit');
        Route::post('update/{id}', 'DoctorController@update')->name('doctor.update');
        Route::delete('delete/{id}', 'DoctorController@destroy')->name('doctor.destroy');
    });

});

    Route::prefix('drug-receive')->middleware(['permission:menu-drug-receive'])->group(function(){
        Route::get('', 'DrugReceiveController@index')->name('drugReceive.index');
        Route::get('create', 'DrugReceiveController@create')->name('drugReceive.create');
        Route::post('store', 'DrugReceiveController@store')->name('drugReceive.store');

    });

    Route::prefix('drug-in')->middleware(['permission:menu-drug-distribution'])->group(function(){
        Route::get('', 'DrugInController@index')->name('drugIn.index');
        Route::get('create', 'DrugInController@create')->name('drugIn.create');
        Route::post('store', 'DrugInController@store')->name('drugIn.store');

    });

    Route::prefix('drug-out')->middleware(['permission:menu-drug-out'])->group(function(){
        Route::get('', 'DrugOutController@index')->name('drugOut.index');
        Route::get('detail/{id}', 'DrugOutController@detail')->name('drugOut.detail');
        Route::post('create-resep', 'DrugOutController@createRecipe')->name('drugOut.createRecipe');
        Route::get('create', 'DrugOutController@create')->name('drugOut.create');
        Route::post('store', 'DrugOutController@store')->name('drugOut.store');

    });

    Route::prefix('report')->group(function () 
    {
        Route::prefix('lplpo')->middleware(['permission:menu-drug-out'])->group(function(){
            Route::get('', 'ReportLPLPOController@index')->name('reportLPLPO.index');
            Route::get('data', 'ReportLPLPOController@data')->name('reportLPLPO.data');
            Route::get('detail/{id}', 'ReportLPLPOController@detail')->name('reportLPLPO.detail');
            Route::get('export', 'ReportLPLPOController@export')->name('reportLPLPO.export');

        });

        Route::prefix('out')->middleware(['permission:menu-drug-out'])->group(function(){
            Route::get('', 'ReportOutController@index')->name('reportOut.index');
            Route::get('data', 'ReportOutController@data')->name('reportOut.data');
            Route::get('detail/{id}', 'ReportOutController@detail')->name('reportOut.detail');
            Route::get('export', 'ReportOutController@export')->name('reportOut.export');
            Route::delete('destroy/{id}', 'ReportOutController@destroy')->name('reportOut.destroy');

        });

        Route::prefix('distribution')->middleware(['permission:menu-drug-out'])->group(function(){
            Route::get('', 'ReportDistributionController@index')->name('reportDistribution.index');
            Route::get('data', 'ReportDistributionController@data')->name('reportDistribution.data');
            Route::get('detail/{id}', 'ReportDistributionController@detail')->name('reportDistribution.detail');
            Route::get('export', 'ReportDistributionController@export')->name('reportDistribution.export');

        });

        Route::prefix('receive')->middleware(['permission:menu-drug-out'])->group(function(){
            Route::get('', 'ReportReceiveController@index')->name('reportReceive.index');
            Route::get('data', 'ReportReceiveController@data')->name('reportReceive.data');
            Route::get('edit/{id}', 'ReportReceiveController@edit')->name('reportReceive.edit');
            Route::get('detail/{id}', 'ReportReceiveController@detail')->name('reportReceive.detail');
            Route::get('export', 'ReportReceiveController@export')->name('reportReceive.export');
            Route::post('update/{id}', 'ReportReceiveController@update')->name('reportReceive.update');
            Route::delete('destroy/{id}', 'ReportReceiveController@destroy')->name('reportReceive.destroy');

        });
    });





