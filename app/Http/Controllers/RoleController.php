<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


use App\Models\Role;
use App\Models\Permission;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $breadcrumbActions = $this->breadcrumbAction();
        $msg = $request->session()->get('message');
        return view('role.index',compact('msg','breadcrumbActions'));
    }

    public function data()
    {
        if(request()->ajax())
        {
            $is_super_admin = Auth::user()->is_super_admin;
            if($is_super_admin) $data = Role::orderby('created_at','desc');
            else  $data = Role::where('name','!=','super-admin')->orderby('created_at','desc');
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('role._action', [
                    'model'      => $data,
                    'edit_modal' => route('role.edit',$data->id),
                    'delete'     => route('role.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $permissions = Permission::pluck('name', 'id')->all();
        return view('role.create',compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Role::where('name',str_slug($request->name))->exists()) return response()->json(['message' => 'Role sudah ada, silahkan cari nama Role lain.'], 422);

        try
        {
            DB::beginTransaction();
            $role = Role::firstorCreate([
                'name' => str_slug($request->name),
                'display_name' => $request->name,
                'description' => $request->description
            ]);
    
            $mappings =  json_decode($request->mappings);
            $array = array();
    
            foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];
    
            if($role->save()) $role->attachPermissions($array);
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function storePermission(Request $request)
    {
        $role = Role::find($request->role_id);
        $role->attachPermissions([$request->permission_id]);
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $role           = Role::find($id);
        $permissions    = Permission::pluck('name', 'id')->all();
        return view('role.edit',compact('role','permissions'));
    }

    public function dataPermission(Request $request,$id)
    {
        if(request()->ajax())
        {
            $data = db::select(db::raw("select permissions.id as id
            ,permissions.display_name
            from roles
            join permission_role on permission_role.role_id = roles.id
            join permissions on permission_role.permission_id = permissions.id
            where roles.id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('role.destroyPermissionRole',[$id,($data)?$data->id : null]),
                ]);


            })
            ->make(true);
        }

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Role::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        try
        {
            DB::beginTransaction();
            $role = Role::find($id);
            $role->name = str_slug($request->name);
            $role->display_name = $request->name;
            $role->description = $request->description;
            $role->save();
            
            DB::commit();
            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function destroy($id)
    {
        $role = Role::findorFail($id)->delete();
        return response()->json(200);
    }

    public function destroyPermissionRole($role_id,$permission_id)
    {
        $role = Role::find($role_id);
        $permissions = $role->permissions()->where('permission_id','!=',$permission_id)->get();
        $array = array();
        foreach ($permissions as $key => $permission) {
            $array [] = $permission->id;
        }

        $role->perms()->sync([]);
        $role->attachPermissions($array);

        return response()->json($permissions);
    }

    static function breadcrumbAction()
    {
        $settings           = array();

        $obj1               = new stdClass();
        $obj1->url          = route('role.create');
        $obj1->iconClass    = 'icon-plus2';
        $obj1->name         = 'Create';
        $settings []        = $obj1;

        return $settings;
    }
}
