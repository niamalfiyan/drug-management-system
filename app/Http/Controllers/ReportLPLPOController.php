<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;
use App\Models\Doctor;
use App\Models\Recipe;
use App\Models\Locator;
use App\Models\Transaction;
use App\Models\DrugReceive;

class ReportLPLPOController extends Controller
{
    public function index(Request $request)
    {
        return view('report_lplpo.index');
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = DB::table('summary_stock')->get();
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('report_lplpo._action', [
                    'model' => $data,
                    'detail' => route('reportLPLPO.detail',$data->id),
                ]);
            })
            ->make(true);
        }
    }
    public function export(Request $request)
    {
        $_start_date = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d');
        $_end_date   = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d');
        $_start_date = $_start_date.' 00:00:00';
        $_end_date = $_end_date.' 23:59:59';
        
        $report_lplpo = DB::select("select drugs.id,
        drugs.`name`,
        total_receive.satuan,
        COALESCE(stock_awal.stock_awal,0) as stock_awal,
        COALESCE(total_receive.qty_receive,0) as qty_receive,
        COALESCE(total_out.qty_out,0) as qty_out,
        COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_receive,0) as persediaan,
        (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_receive,0) - COALESCE(total_out.qty_out,0)) as sisa,
        COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
        COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_receive,0) - COALESCE(total_out.qty_out,0)) as permintaan,
        COALESCE(total_apbd_kota.qty_receive,0) as qty_apbd_kota,
        COALESCE(total_apbd_provinsi.qty_receive,0) as qty_apbd_provinsi,
        COALESCE(total_dak.qty_receive,0) as qty_dak
        from drugs
        left join (select drug_receives.drug_id,
        sum(case 
                when transactions.`status` ='out' then
                transactions.qty * -1
                else
                transactions.qty
        end ) as stock_awal
        from drug_receives
        left join transactions on transactions.drug_receive_id = drug_receives.id
        where transactions.created_at < '".$_start_date."' and transactions.status not in ('in', 'retur')
        GROUP BY drug_receives.drug_id
        ) stock_awal on drugs.id = stock_awal.drug_id
        left join
        (
            select drug_receives.drug_id,
            drug_receives.satuan,
            sum(qty_receive) as qty_receive
            from drug_receives
            where created_at >= '".$_start_date."' and created_at <= '".$_end_date."'
            GROUP BY drug_receives.drug_id,drug_receives.satuan
        ) total_receive on drugs.id = total_receive.drug_id
        left join (
        select drug_receives.drug_id,
        sum(transactions.qty) as qty_out
        from transactions
        join drug_receives on transactions.drug_receive_id = drug_receives.id
        where transactions.`status` ='out' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."'
        GROUP BY drug_receives.drug_id
        ) total_out on drugs.id = total_out.drug_id
        left join
        (
            select drug_receives.drug_id,
            sum(qty_receive) as qty_receive
            from drug_receives
            where asal ='apbd kota' and created_at >= '".$_start_date."' and created_at <= '".$_end_date."'
            GROUP BY drug_receives.drug_id
        ) total_apbd_kota on drugs.id = total_apbd_kota.drug_id
        left join
        (
            select drug_receives.drug_id,
            sum(qty_receive) as qty_receive
            from drug_receives
            where asal ='apbd provinsi' and created_at >= '".$_start_date."' and created_at <= '".$_end_date."'
            GROUP BY drug_receives.drug_id
        ) total_apbd_provinsi on drugs.id = total_apbd_provinsi.drug_id
        left join
        (
            select drug_receives.drug_id,
            sum(qty_receive) as qty_receive
            from drug_receives
            where asal ='dak' and created_at >= '".$_start_date."' and created_at <= '".$_end_date."'
            GROUP BY drug_receives.drug_id
        ) total_dak on drugs.id = total_dak.drug_id
        
        ");
        
        //dd($jumlah_resep);
        $file_name = 'REPORT_LPLPO'.'_FROM_'.$_start_date.'_TO_'.$_end_date;
    
        return Excel::create($file_name,function($excel) use ($report_lplpo,$_start_date,$_end_date)
        {
            $excel->sheet('ACTIVE',function($sheet)use($report_lplpo,$_start_date,$_end_date)
            {
                $sheet->mergeCells('A1:A2');
                $sheet->setCellValue('A1','NAMA OBAT');
                $sheet->mergeCells('B1:B2');
                $sheet->setCellValue('B1','SATUAN');
                $sheet->mergeCells('C1:C2');
                $sheet->setCellValue('C1','STOK AWAL');
                $sheet->mergeCells('D1:D2');
                $sheet->setCellValue('D1','PENERIMAAN');
                $sheet->mergeCells('E1:E2');
                $sheet->setCellValue('E1','PERSEDIAAN');
                $sheet->mergeCells('F1:F2');
                $sheet->setCellValue('F1','PEMAKAIAN');
                $sheet->mergeCells('G1:G2');
                $sheet->setCellValue('G1','SISA STOK');
                $sheet->mergeCells('H1:H2');
                $sheet->setCellValue('H1','STOK OPTIMUM');
                $sheet->mergeCells('I1:I2');
                $sheet->setCellValue('I1','PERMINTAAN');
                $sheet->mergeCells('J1:L1');
                $sheet->setCellValue('J1','PEMBERIAN');
                $sheet->setCellValue('J2','APBD KOTA');
                $sheet->setCellValue('K2','APBD PROVINSI');
                $sheet->setCellValue('L2','DAK');

                $sheet->setAutoSize(true);
                    $sheet->cell('A1:L1', function($cell) {
                        $cell->setBackground('#808080');
                        $cell->setValignment('center');
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('J2:L2', function($cell) {
                        $cell->setBackground('#808080');
                        $cell->setValignment('center');
                        $cell->setAlignment('center');
                    });


            
            $row=3;

           
            foreach ($report_lplpo as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->name);
                $sheet->setCellValue('B'.$row,$i->satuan);
                $sheet->setCellValue('C'.$row,$i->stock_awal);
                $sheet->setCellValue('D'.$row,$i->qty_receive);
                $sheet->setCellValue('E'.$row,$i->persediaan);
                $sheet->setCellValue('F'.$row,$i->qty_out);
                $sheet->setCellValue('G'.$row,$i->sisa);
                $sheet->setCellValue('H'.$row,$i->stok_optimum);
                if($i->permintaan < 0)
                {
                    $total_permintaan = 0;
                }
                else
                {
                    $total_permintaan = $i->permintaan;
                }
                $sheet->setCellValue('I'.$row,$total_permintaan);
                // $sheet->setCellValue('J'.$row,$i->qty_apbd_kota);
                // $sheet->setCellValue('K'.$row,$i->qty_apbd_provinsi);
                // $sheet->setCellValue('L'.$row,$i->qty_dak);
                $row++;
            }

            $sheet->mergeCells('A'.($row+2).':C'.($row+2));
            $sheet->setCellValue('A'.($row+2),'MENGETAHUI');
            $sheet->mergeCells('A'.($row+3).':C'.($row+3));
            $sheet->setCellValue('A'.($row+3),'a.n KEPALA DINAS KESEHATAN');
            $sheet->mergeCells('A'.($row+4).':C'.($row+4));
            $sheet->setCellValue('A'.($row+4),'KOTA SURAKARTA');
            $sheet->mergeCells('A'.($row+5).':C'.($row+5));
            $sheet->setCellValue('A'.($row+5),'KEPALA BIDANG DATA DAN SUMBER DAYA KESEHATAN');
            $sheet->mergeCells('A'.($row+9).':C'.($row+9));
            $sheet->setCellValue('A'.($row+9),'dr. SRI RAHAYU SUSILOWATI');
            $sheet->mergeCells('A'.($row+10).':C'.($row+10));
            $sheet->setCellValue('A'.($row+10),'Pembina');

            $sheet->cell('A'.($row+2).':A'.($row+10), function($cell) {
                $cell->setAlignment('center');
            });

            $sheet->mergeCells('E'.($row+2).':G'.($row+2));
            $sheet->setCellValue('E'.($row+2),'YANG MENYERAHKAN');
            $sheet->mergeCells('E'.($row+3).':G'.($row+3));
            $sheet->setCellValue('E'.($row+3),'KEPALA UPT INSTALASI FARMASI');
            $sheet->mergeCells('E'.($row+4).':G'.($row+4));
            $sheet->setCellValue('E'.($row+4),'DINAS KESEHATAN KOTA SURAKARTA');
            $sheet->mergeCells('E'.($row+5).':G'.($row+5));
            $sheet->setCellValue('E'.($row+5),'');
            $sheet->mergeCells('E'.($row+9).':G'.($row+9));
            $sheet->setCellValue('E'.($row+9),'HERU CAHYONO, S,Si.,Apt.');
            $sheet->mergeCells('E'.($row+10).':G'.($row+10));
            $sheet->setCellValue('E'.($row+10),'Pembina');

            $sheet->cell('E'.($row+2).':E'.($row+10), function($cell) {
                $cell->setAlignment('center');
            });

            $sheet->mergeCells('I'.($row+2).':K'.($row+2));
            $sheet->setCellValue('I'.($row+2),'YANG MELAPORKAN');
            $sheet->mergeCells('I'.($row+3).':K'.($row+3));
            $sheet->setCellValue('I'.($row+3),'KEPALA UPT PUSKESMAS JAYENGAN');
            $sheet->mergeCells('I'.($row+4).':K'.($row+4));
            $sheet->setCellValue('I'.($row+4),'DINAS KESEHATAN KOTA SURAKARTA');
            $sheet->mergeCells('I'.($row+5).':K'.($row+5));
            $sheet->setCellValue('I'.($row+5),'');
            $sheet->mergeCells('I'.($row+9).':K'.($row+9));
            $sheet->setCellValue('I'.($row+9),'dr. ANJANG KUSUMANETRA');
            $sheet->mergeCells('I'.($row+10).':K'.($row+10));
            $sheet->setCellValue('I'.($row+10),'Penata Tk.1');

            $sheet->mergeCells('M'.($row+2).':O'.($row+2));
            $sheet->setCellValue('M'.($row+2),'YANG MENERIMA');

            $sheet->cell('I'.($row+2).':I'.($row+10), function($cell) {
                $cell->setAlignment('center');
                
            });

            $sheet->prependRow(1, array(
                ''
            ));
            $sheet->prependRow(1, array(
                ''
            ));
            $sheet->prependRow(1, array(
                ''
            ));
            $sheet->prependRow(1, array(
                ''
            ));
            $sheet->mergeCells('A1:K1');
            $sheet->setCellValue('A1','LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT (LPLPO)');
            $sheet->cell('A1:K1', function($cell) {
                $cell->setAlignment('center');
                
            });

            $sheet->mergeCells('A2:K2');
            $sheet->setCellValue('A2','PUSKESMAS JAYENGAN');
            $sheet->cell('A2:K2', function($cell) {
                $cell->setAlignment('center');
                
            });

            $resep = DB::select("select count(0) as jumlah from recipes where created_at >= '".$_start_date."' and created_at <= '".$_end_date."'");
            $jumlah_resep = $resep[0];
            
            $sheet->setCellValue('K4','Jumlah Resep : '.$jumlah_resep->jumlah);

            });

        })
        ->export('xlsx');
    }
    
}
