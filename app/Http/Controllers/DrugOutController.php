<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;
use App\Models\DrugIn;
use App\Models\Doctor;
use App\Models\Recipe;
use App\Models\Locator;
use App\Models\Transaction;
use App\Models\DrugReceive;

class DrugOutController extends Controller
{
    public function index(Request $request)
    {
        // if($request->session()->has('message')) 
        // $request->session()->forget('message');
        $doctors = Doctor::pluck('name', 'id')->all();
        return view('drug_out.index',compact('doctors'));
    }

    public function createRecipe(Request $request)
    {
            $recipe        = Recipe::Create([
            'nama_pasien'        => $request->nama_pasien,
            'alamat'             => $request->alamat,
            'jenis_kelamin'      => $request->jenis_kelamin,
            'diagnosis'          => $request->diagnosis,
            'berat_badan'        => $request->berat_badan,
            'tekanan_darah'      => $request->tekanan_darah,
            'status_jaminan'     => $request->status_jaminan,
            'tempat_pemeriksaan' => $request->tempat_pemeriksaan,
            'tanggal_resep'      => Carbon::createFromFormat('d/m/Y', $request->tanggal_resep)->format('Y-m-d'),
            'tanggal_lahir'      => Carbon::createFromFormat('d/m/Y', $request->tanggal_lahir)->format('Y-m-d'),
            'user_id'            => Auth::user()->id,
            'doctor_id'          => $request->doctor_id,
            ]);

            $recipe_id = $recipe->id;

            return redirect()->route('drugOut.detail', [$recipe_id]);


    }

    public function detail($id)
    {
        $drugs = Drug::pluck('name', 'id')->all();
        $recipe = Recipe::find($id);
        $from_locators = Locator::where('name', '!=', 'OUT')->pluck('name', 'id')->all();
        return view('drug_out.detail',compact(['drugs','recipe','from_locators']));
    }
    public function create(Request $request)
    {
        $locator = Locator::where('name', 'RECEIVE')->first();
        $locator_from = $locator->id;
        $_locator_from = $request->locator_from;

        $id_obat   = $request->name;
        $id_obat   = $request->name;
        $qty       = $request->receive_qty;
        $recipe_id = $request->recipe_id;
        $recipe    = Recipe::find($recipe_id);
        //dd($recipe,$recipe_id);
        if($locator_from == $_locator_from)
        {
            $data_receive = DrugReceive::where('drug_id', $id_obat)
                                        ->where('stock', '>', 0);

            $max_date     = $data_receive->orderBy('expired_date', 'asc')->first()->expired_date;
        }
        else
        {
            $data_receive = DrugIn::where('drug_id', $id_obat)
                            ->where('locator_id', $_locator_from)
                            ->where('stock', '>', 0);

            $max_date     = $data_receive->orderBy('expired_date', 'asc')->first()->expired_date;

        }
        $today        = Carbon::now();
        $ts2 = strtotime($max_date);
        $ts1 = strtotime($today);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
        //sisa expired
        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

        $stock        = $data_receive->sum('stock');
        if($stock - $qty < 0)
        {
            return response()->json('Qty yang tersisa tinggal '.$stock,422);
        }
        $obat         = Drug::find($id_obat);
        $nama         = $obat->name;
        $code         = $obat->code;
        if($recipe->status_jaminan == 'gratis')
        {
            $harga = 0;
        }
        else
        {
            $harga  = $request->price;
        }
        $dosis       = $request->dosis;
        $racikan       = $request->racikan;
        //$total        = $qty * $harga;

        $array = [
            'drug_id'    => $id_obat,
            'nama'       => $nama,
            'code'       => $code,
            'qty'        => $qty,
            'dosis'      => $dosis,
            'racikan'    => $racikan,
            'diff_month' => $diff,
            'locator_from' => $_locator_from

        ];

        //dd($array);

        return response()->json($array,200);
    }

    public function store(Request $request)
    {
        $locator  = Locator::where('name', 'RECEIVE')->first();
        $locator_from  = $locator->id;
        $recipe_id     = $request->recipe_id;
        $list_outs     = json_decode($request->list_receive);
        
        try
        {
            DB::beginTransaction();

            $locator_id = Locator::where('name', 'OUT')->first()->id;
            foreach($list_outs as $key => $list_out)
            {
                $qty_need = $list_out->qty;
                $user_id  = Auth::user()->id;
                $drug_id  = $list_out->drug_id;
                $dosis    = $list_out->dosis;
                $racikan  = $list_out->racikan;
                $_locator_from = $list_out->locator_from;

                //cek asal
                if($locator_from == $_locator_from)
                {   
                    $drug_receives = DrugReceive::where([
                                    ['drug_id', $drug_id],
                                    ['stock', '>', 0],
                                    ])->whereNull('deleted_at')
                                    ->orderBy('expired_date', 'asc')
                                    ->orderBy('stock', 'asc')
                                    ->get();
                }
                else
                {
                    $drug_receives = DrugIn::where([
                                    ['drug_id', $drug_id],
                                    ['locator_id', $_locator_from],
                                    ['stock', '>', 0],
                                    ])->whereNull('deleted_at')
                                    ->orderBy('expired_date', 'asc')
                                    ->orderBy('stock', 'asc')
                                    ->get();

                }
                // dd($locator_from,$_locator_from);
                if(count($drug_receives) == 0)
                {
                    return response()->json('Terjadi Kesalahan '.$stock,422);
                }
                foreach($drug_receives as $key => $drug_receive)
                {
                    $stock           = $drug_receive->stock;
                    $reserved        = $drug_receive->reserved;

                    if($locator_from == $_locator_from)
                    {
                        $drug_in_id = null;
                        $drug_receive_id = $drug_receive->id;
                    }
                    else
                    {
                        $drug_in_id =  $drug_receive->id;
                        $drug_receive_id = $drug_receive->drug_receive_id;
                    }
                    //jika qty receive cukup untuk memenuhi kebutuhan out
                    if($stock - $qty_need > 0)
                    {
                        $qty_supply = $qty_need;
                        Transaction::Create([
                            'drug_receive_id' => $drug_receive_id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'out',
                            'note'            => null,
                            'locator_id'      => $locator_id,
                            'recipe_id'       => $recipe_id,
                            'dosis'           => $dosis,
                            'racikan'         => $racikan,
                            'drug_in_id'         => $drug_in_id,
                        ]);
                        $qty_need                 = 0;
                        $stock                    -= $qty_supply;
                        $reserved                 += $qty_supply;
                        if($locator_from == $_locator_from)
                        {
                            $update_receive           = DrugReceive::find($drug_receive_id);
                        }
                        else
                        {
                            $update_receive           = DrugIn::find($drug_in_id);
                        }
                        $update_receive->stock    = $stock;
                        $update_receive->reserved = $reserved;
                        $update_receive->save();
                        break;
                    }
                    else
                    {
                        $qty_supply = $stock;
                        Transaction::Create([
                            'drug_receive_id' => $drug_receive_id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'out',
                            'note'            => null,
                            'locator_id'      => $locator_id,
                            'recipe_id'       => $recipe_id,
                            'dosis'           => $dosis,
                            'racikan'         => $racikan,
                            'drug_in_id'         => $drug_in_id,
                        ]);
                        $qty_need                 -= $qty_supply;
                        $stock                    -= $qty_supply;
                        $reserved                 += $qty_supply;
                        if($locator_from == $_locator_from)
                        {
                            $update_receive           = DrugReceive::find($drug_receive_id);
                        }
                        else
                        {
                            $update_receive           = DrugIn::find($drug_in_id);
                        }
                        $update_receive->stock    = $stock;
                        $update_receive->reserved = $reserved;
                        $update_receive->save();
                        continue;

                    }
                }
             }
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

}
