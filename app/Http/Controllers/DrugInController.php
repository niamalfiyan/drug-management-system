<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;
use App\Models\DrugIn;
use App\Models\Locator;
use App\Models\Transaction;
use App\Models\DrugReceive;

use App\Http\Controllers\Controller;

class DrugInController extends Controller
{
    public function index(Request $request)
    {
        $drugs         = Drug::pluck('name', 'id')->all();
        $from_locators = Locator::where('name', '!=', 'OUT')->pluck('name', 'id')->all();
        $locators      = Locator::where('name', '!=', 'RECEIVE')->where('name', '!=', 'OUT')->pluck('name', 'id')->all();
        // if($request->session()->has('message')) $request->session()->forget('message');
        return view('drug_in.index',compact(['drugs','locators', 'from_locators']));
    }

    public function create(Request $request)
    {
        $id_obat       = $request->name;
        $is_retur      = $request->is_retur;
        $locator_id    = $request->locator_id;
        $expired_date  = Carbon::createFromFormat('d/m/Y', $request->expired_date)->format('Y-m-d');
        $note          = $request->note;
        $qty           = $request->qty_in;
        $locator_from  = Locator::where('name', 'RECEIVE')->first();
        $_locator_from = $request->locator_from;

        if($_locator_from == $locator_from->id)
        {
            $data_receive         = DrugReceive::where([
                                        ['drug_id',$id_obat],
                                        [db::raw('date(expired_date)'),$expired_date]
                                        ]
                                    )->first();
            //dd($data_receive);
            if($data_receive)
            {
                $drug_receive_id = $data_receive->id;
                $qty_receive     = $data_receive->stock;
                if($qty_receive - $qty < 0)
                {
                    return response()->json('Qty yang tersisa di penerimaan tinggal '.$qty_receive,422);
                }
                $nama         = $data_receive->Drug->name;
                $code         = $data_receive->Drug->code;
                $expired_date = $data_receive->expired_date;

                $array = [
                    'drug_receive_id' => $drug_receive_id,
                    'locator_id'      => $locator_id,
                    'nama'            => $nama,
                    'code'            => $code,
                    'expired_date'    => $expired_date,
                    'note'            => $note,
                    'qty'             => $qty,
                    'locator_from'    => $_locator_from

                ];
            }
            else
            {
                return response()->json('Data Tidak Ditemukan',422);
            }
        }
        else
        {
            $data_receive         = DrugIn::where([
                ['drug_id',$id_obat],
                [db::raw('date(expired_date)'),$expired_date],
                ['locator_id', $_locator_from],
                ]
            )->first();
            //dd($data_receive);
            if($data_receive)
            {
            $drug_receive_id = $data_receive->id;
            $qty_receive     = $data_receive->stock;
            if($qty_receive - $qty < 0)
            {
            return response()->json('Qty yang tersisa di distribusi tinggal '.$qty_receive,422);
            }
            $nama         = $data_receive->Drug->name;
            $code         = $data_receive->Drug->code;
            $expired_date = $data_receive->expired_date;

            $array = [
            'drug_receive_id' => $drug_receive_id,
            'locator_id'      => $locator_id,
            'nama'            => $nama,
            'code'            => $code,
            'expired_date'    => $expired_date,
            'note'            => $note,
            'qty'             => $qty,
            'locator_from'    => $_locator_from

            ];
            }
            else
            {
            return response()->json('Data Tidak Ditemukan',422);
            }

        }

        return response()->json($array,200);

    }

    public function store(Request $request)
    {
        $list_ins           = json_decode($request->list_receive);
        //dd($list_ins);
        try
        {
            DB::beginTransaction();
            foreach($list_ins as $key => $list_in)
            {
                $locator_from    = Locator::where('name', 'RECEIVE')->first();
                $_locator_from   = $list_in->locator_from;
                $qty_need        = $list_in->qty;
                $user_id         = Auth::user()->id;
                $drug_receive_id = $list_in->drug_receive_id;
                $locator_id      = $list_in->locator_id;

                //var_dump($locator_from->id, $_locator_from);
                //reguler
                if($_locator_from == $locator_from->id)
                {
                $is_retur     = 0;
                $drug_receive = DrugReceive::where([
                                 ['id', $drug_receive_id],
                                 ['stock', '>', 0],
                ])->whereNull('deleted_at')
                ->first();
                //dd($drug_receive);

                // foreach($drug_receives as $key => $drug_receive)
                // {
                    $drug_receive_id = $drug_receive->id;
                    $stock           = $drug_receive->stock;
                    $reserved        = $drug_receive->reserved;
                    $drug_id         = $drug_receive->drug_id;
                    $expired_date    = $drug_receive->expired_date;
                    //jika qty receive cukup untuk memenuhi kebutuhan out
                    if($stock - $qty_need > 0)
                    {
                        $qty_supply = $qty_need;

                        $drug_in = DrugIn::Create([
                            'drug_receive_id' => $drug_receive->id,
                            'user_id'         => Auth::user()->id,
                            'qty_in'          => $qty_supply,
                            'stock'           => $qty_supply,
                            'locator_id'      => $locator_id,
                            'drug_receive_id' => $drug_receive->id,
                            'reserved'        => 0,
                            'note'            => $drug_receive->note,
                            'is_retur'        => $is_retur,
                            'drug_id'         => $drug_id,
                            'expired_date'    => $expired_date,
                        ]);
                        //var_dump($drug_in->id);
                        $Transaction_in = Transaction::Create([
                            'drug_receive_id' => $drug_receive->id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'in',
                            'note'            => null,
                            'locator_id'      => $locator_id,
                            'recipe_id'       => null,
                            'is_retur'        => $is_retur,
                            'drug_in_id'      => $drug_in->id,
                        ]);
                        //var_dump($Transaction_in->id);
                        $qty_need                 = 0;
                        $stock                    -= $qty_supply;
                        $reserved                 += $qty_supply;
                        $update_receive           = DrugReceive::find($drug_receive_id);
                        $update_receive->stock    = $stock;
                        $update_receive->reserved = $reserved;
                        $update_receive->save();
                        //break;
                    }
                    else
                    {
                        $qty_supply = $stock;

                        $drug_in = DrugIn::Create([
                            'drug_receive_id' => $drug_receive->id,
                            'user_id'         => Auth::user()->id,
                            'qty_in'          => $qty_supply,
                            'stock'           => $qty_supply,
                            'locator_id'      => $locator_id,
                            'drug_receive_id' => $drug_receive->id,
                            'reserved'        => 0,
                            'note'            => $drug_receive->note,
                            'is_retur'        => $is_retur,
                            'drug_id'         => $drug_id,
                            'expired_date'    => $expired_date,
                        ]);

                        $Transaction_in = Transaction::Create([
                            'drug_receive_id' => $drug_receive->id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'in',
                            'note'            => null,
                            'locator_id'      => $locator_id,
                            'recipe_id'       => null,
                            'is_retur'        => $is_retur,
                            'drug_in_id'      => $drug_in->id,
                        ]);
                        //var_dump($drug_receive->id);
                        $qty_need                 -= $qty_supply;
                        $stock                    -= $qty_supply;
                        $reserved                 += $qty_supply;
                        $update_receive           = DrugReceive::find($drug_receive_id);
                        $update_receive->stock    = $stock;
                        $update_receive->reserved = $reserved;
                        $update_receive->save();
                        //continue;

                    }
                }
                //retur
                else
                {
                    $is_retur     = 1;
                $drug_receive = DrugIn::where([
                                 ['id', $drug_receive_id],
                                 ['locator_id', $_locator_from],
                                 ['stock', '>', 0],
                ])->whereNull('deleted_at')
                ->first();
                //dd($drug_receive);

                // foreach($drug_receives as $key => $drug_receive)
                // {
                    $drug_receive_id = $drug_receive->id;
                    $stock           = $drug_receive->stock;
                    $reserved        = $drug_receive->reserved;
                    $drug_id         = $drug_receive->drug_id;
                    $expired_date    = $drug_receive->expired_date;
                    $locator_retur   = $drug_receive->locator_id;
                    //jika qty receive cukup untuk memenuhi kebutuhan out
                    if($stock - $qty_need > 0)
                    {
                        $qty_supply = $qty_need;

                        $drug_in = DrugIn::Create([
                            'drug_receive_id' => $drug_receive->drug_receive_id,
                            'user_id'         => Auth::user()->id,
                            'qty_in'          => $qty_supply,
                            'stock'           => $qty_supply,
                            'locator_id'      => $locator_id,
                            //'drug_receive_id' => $drug_receive->id,
                            'reserved'        => 0,
                            'note'            => $drug_receive->note,
                            'is_retur'        => $is_retur,
                            'drug_id'         => $drug_id,
                            'expired_date'    => $expired_date,
                        ]);
                        //var_dump($drug_in->id);
                        $Transaction_retur = Transaction::Create([
                            'drug_receive_id' => $drug_receive->drug_receive_id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'retur',
                            'note'            => null,
                            'locator_id'      => $locator_retur,
                            'recipe_id'       => null,
                            'is_retur'        => $is_retur,
                            'drug_in_id'      => $drug_receive_id,
                        ]);

                        $Transaction_in = Transaction::Create([
                            'drug_receive_id' => $drug_receive->drug_receive_id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'in',
                            'note'            => null,
                            'locator_id'      => $locator_id,
                            'recipe_id'       => null,
                            'is_retur'        => $is_retur,
                            'drug_in_id'      => $drug_in->id,
                        ]);
                        //var_dump($Transaction_in->id);
                        $qty_need                 = 0;
                        $stock                    -= $qty_supply;
                        $reserved                 += $qty_supply;
                        $update_receive           = DrugIn::find($drug_receive_id);
                        $update_receive->stock    = $stock;
                        $update_receive->reserved = $reserved;
                        $update_receive->save();
                        //break;
                    }
                    else
                    {
                        $qty_supply = $stock;

                        $drug_in = DrugIn::Create([
                            //'drug_receive_id' => $drug_receive->drug_receive_id,
                            'user_id'         => Auth::user()->id,
                            'qty_in'          => $qty_supply,
                            'stock'           => $qty_supply,
                            'locator_id'      => $locator_id,
                            'drug_receive_id' => $drug_receive->id,
                            'reserved'        => 0,
                            'note'            => $drug_receive->note,
                            'is_retur'        => $is_retur,
                            'drug_id'         => $drug_id,
                            'expired_date'    => $expired_date,
                        ]);

                        //retur
                        $Transaction_retur = Transaction::Create([
                            'drug_receive_id' => $drug_receive->drug_receive_id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'retur',
                            'note'            => null,
                            'locator_id'      => $locator_retur,
                            'recipe_id'       => null,
                            'is_retur'        => $is_retur,
                            'drug_in_id'      => $drug_receive_id,
                        ]);

                        $Transaction_in = Transaction::Create([
                            'drug_receive_id' => $drug_receive->drug_receive_id,
                            'user_id'         => $user_id,
                            'qty'             => $qty_supply,
                            'status'          => 'in',
                            'note'            => null,
                            'locator_id'      => $locator_id,
                            'recipe_id'       => null,
                            'is_retur'        => $is_retur,
                            'drug_in_id'      => $drug_in->id,
                        ]);
                        //var_dump($drug_receive->id);
                        $qty_need                 -= $qty_supply;
                        $stock                    -= $qty_supply;
                        $reserved                 += $qty_supply;
                        $update_receive           = DrugIn::find($drug_receive_id);
                        $update_receive->stock    = $stock;
                        $update_receive->reserved = $reserved;
                        $update_receive->save();
                        //continue;

                    }
                }
                // }
             }
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        
    }
    
}
