<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;
use App\Models\DrugIn;
use App\Models\Doctor;
use App\Models\Recipe;
use App\Models\Locator;
use App\Models\Transaction;
use App\Models\DrugReceive;

class ReportOutController extends Controller
{
    public function index(Request $request)
    {
        return view('report_out.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $data = DB::table('report_out')->whereBetween('created_at',[$start_date,$end_date])->get();
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('report_out._action', [
                    'model' => $data,
                    'delete' => route('reportOut.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $_start_date = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d');
        $_end_date   = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d');
        $_start_date = $_start_date.' 00:00:00';
        $_end_date = $_end_date.' 23:59:59';

        $report_out =DB::table('report_out')->where('created_at','>=',$_start_date)
        ->where('created_at','<=',$_end_date)
        ->get();   

        $resep_total = DB::select("select drug_receives.drug_id from transactions
        join drug_receives on transactions.drug_receive_id = drug_receives.id
        join drugs on drug_receives.drug_id = drugs.id
        where drugs.type ='generik' and transactions.`status`='out' and recipe_id is not null and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."'
        GROUP BY drug_id");

        $jumlah_obat  = count($resep_total);
        $resep = DB::select("select drug_receives.drug_id from transactions
        join drug_receives on transactions.drug_receive_id = drug_receives.id
        join drugs on drug_receives.drug_id = drugs.id
        where drugs.type ='generik' and transactions.`status`='out' and recipe_id is not null and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."'
        GROUP BY drug_id");

        
        $jumlah_resep = count($resep);     //dd($jumlah_resep);
        $presentase   = $jumlah_resep/$jumlah_obat *100;
        $file_name    = 'REPORT_OUT'.'_FROM_'.$_start_date.'_TO_'.$_end_date;
    
        return Excel::create($file_name,function($excel) use ($report_out,$_start_date,$_end_date,$presentase)
        {
            $excel->sheet('ACTIVE',function($sheet)use($report_out,$_start_date,$_end_date,$presentase)
            {
                $sheet->setCellValue('A1','TANGGAL OUT');
                $sheet->setCellValue('B1','KODE OBAT');
                $sheet->setCellValue('C1','NAMA OBAT');
                $sheet->setCellValue('D1','JENIS');
                $sheet->setCellValue('E1','SATUAN');
                $sheet->setCellValue('F1','QTY OUT');
                $sheet->setCellValue('G1','EXPIRED DATE');
                $sheet->setCellValue('H1','PIC');
                $sheet->setCellValue('I1','NAMA PASIEN');
            
            $row=2;

            foreach ($report_out as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->created_at);
                $sheet->setCellValue('B'.$row,$i->code);
                $sheet->setCellValue('C'.$row,$i->name);
                $sheet->setCellValue('D'.$row,$i->type);
                $sheet->setCellValue('E'.$row,$i->satuan);
                $sheet->setCellValue('F'.$row,$i->qty);
                $sheet->setCellValue('G'.$row,$i->expired_date);
                $sheet->setCellValue('H'.$row,$i->username);
                $sheet->setCellValue('I'.$row,$i->nama_pasien);
                $row++;
            }
            $new_row = $row + 4;
            // $resep = DB::select("select count(0) as jumlah from recipes where created_at >= '".$_start_date."' and created_at <= '".$_end_date."'");
            // $jumlah_resep = $resep[0];

            // $qty_out = DB::select("select count(0) as jumlah from transactions where status ='out' and created_at >= '".$_start_date."' and created_at <= '".$_end_date."'");
            // $jumlah_out = $qty_out[0];
            // $generik  = $jumlah_out/$jumlah_resep;
            $sheet->setCellValue('I'.$new_row, 'PEMAKAIAN OBAT GENERIK :'.$presentase.'%');

            });

        })
        ->export('xlsx');
    }

    public function destroy($id)
    {
        $transaction = Transaction::find($id);
        $drug_in_id = $transaction->drug_in_id;
        $qty_delete = $transaction->qty;

        $drug_in = DrugIn::find($drug_in_id);
        $drug_in->stock = $drug_in->stock + $qty_delete;
        $drug_in->reserved = $drug_in->reserved - $qty_delete;
        $drug_in->save();

        $transaction->delete();
        return response()->json(200);

    }
}
