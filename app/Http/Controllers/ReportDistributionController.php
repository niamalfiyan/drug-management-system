<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;
use App\Models\Doctor;
use App\Models\Recipe;
use App\Models\Locator;
use App\Models\Transaction;
use App\Models\DrugReceive;

class ReportDistributionController extends Controller
{
    public function index(Request $request)
    {
        return view('report_distribution.index');
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = DB::table('summary_stock_per_distribusi')->get();
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('report_distribution._action', [
                    'model' => $data,
                    'detail' => route('reportDistribution.detail',$data->id),
                ]);
            })
            ->make(true);
        }
    }
//     public function export(Request $request)
//     {
//         $_start_date = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d');
//         $_end_date   = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d');
//         $_start_date = $_start_date.' 00:00:00';
//         $_end_date = $_end_date.' 23:59:59';

//         $report_lplpo = DB::select("select puskesmas_induk.*,
// puskesmas_keliling.permintaan as keliling_permintaan,
// puskesmas_keliling.stock_awal as keliling_stock_awal,
// puskesmas_keliling.persediaan as keliling_persediaan,
// puskesmas_keliling.qty_in as keliling_qty_in,
// puskesmas_keliling.qty_out as keliling_qty_out,
// puskesmas_keliling.sisa as keliling_sisa,
// puskesmas_keliling.stok_optimum as keliling_stok_optimum,
// puskesmas_pembantu.permintaan as pembantu_permintaan,
// puskesmas_pembantu.stock_awal as pembantu_stock_awal,
// puskesmas_pembantu.persediaan as pembantu_persediaan,
// puskesmas_pembantu.qty_in as pembantu_qty_in,
// puskesmas_pembantu.qty_out as pembantu_qty_out,
// puskesmas_pembantu.sisa as pembantu_sisa,
// puskesmas_pembantu.stok_optimum as pembantu_stok_optimum from (select drugs.id,
//         drugs.`name`,
//         COALESCE(stock_awal.stock_awal,0) as stock_awal,
//         COALESCE(total_receive.qty_in,0) as qty_in,
//         COALESCE(total_out.qty_out,0) as qty_out,
//         COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
//         (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
//         COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
//         COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
//         from drugs
//         left join (select drug_ins.drug_id,
//         sum(case 
//                 when transactions.`status` ='out' then
//                 transactions.qty * -1
//                 else
//                 transactions.qty
//         end ) as stock_awal
//         from drug_ins
//         left join transactions on transactions.drug_receive_id = drug_ins.id
// 				join locators on drug_ins.locator_id = locators.id
//         where transactions.created_at < '".$_start_date."' and transactions.status not in ('retur','receive') and locators.name ='PUSKESMAS INDUK'
//         GROUP BY drug_ins.drug_id
//         ) stock_awal on drugs.id = stock_awal.drug_id
//         left join
//         (
//             select drug_ins.drug_id,
//             sum(qty_in) as qty_in
//             from drug_ins
// 						join locators on drug_ins.locator_id = locators.id
//             where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS INDUK'
//             GROUP BY drug_ins.drug_id
//         ) total_receive on drugs.id = total_receive.drug_id
//         left join (
//         select drug_ins.drug_id,
//         sum(transactions.qty) as qty_out
//         from transactions
//         join drug_ins on transactions.drug_in_id = drug_ins.id
// 										join locators on drug_ins.locator_id = locators.id
//         where transactions.`status` ='out' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS INDUK'
//         GROUP BY drug_ins.drug_id
//         ) total_out on drugs.id = total_out.drug_id ) puskesmas_induk
// 				left join 
// 				(
// 				select drugs.id,
//         drugs.`name`,
//         COALESCE(stock_awal.stock_awal,0) as stock_awal,
//         COALESCE(total_receive.qty_in,0) as qty_in,
//         COALESCE(total_out.qty_out,0) as qty_out,
//         COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
//         (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
//         COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
//         COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
//         from drugs
//         left join (select drug_ins.drug_id,
//         sum(case 
//                 when transactions.`status` ='out' then
//                 transactions.qty * -1
//                 else
//                 transactions.qty
//         end ) as stock_awal
//         from drug_ins
//         left join transactions on transactions.drug_receive_id = drug_ins.id
// 				join locators on drug_ins.locator_id = locators.id
//         where transactions.created_at < '".$_start_date."' and transactions.status not in ('retur','receive') and locators.name ='PUSKESMAS PEMBANTU'
//         GROUP BY drug_ins.drug_id
//         ) stock_awal on drugs.id = stock_awal.drug_id
//         left join
//         (
//             select drug_ins.drug_id,
//             sum(qty_in) as qty_in
//             from drug_ins
// 						join locators on drug_ins.locator_id = locators.id
//             where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS PEMBANTU'
//             GROUP BY drug_ins.drug_id
//         ) total_receive on drugs.id = total_receive.drug_id
//         left join (
//         select drug_ins.drug_id,
//         sum(transactions.qty) as qty_out
//         from transactions
//         join drug_ins on transactions.drug_in_id = drug_ins.id
// 										join locators on drug_ins.locator_id = locators.id
//         where transactions.`status` ='out' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS PEMBANTU'
//         GROUP BY drug_ins.drug_id
//         ) total_out on drugs.id = total_out.drug_id ) puskesmas_pembantu on puskesmas_induk.id = puskesmas_pembantu.id
// 				left join(
// 				select drugs.id,
//         drugs.`name`,
//         COALESCE(stock_awal.stock_awal,0) as stock_awal,
//         COALESCE(total_receive.qty_in,0) as qty_in,
//         COALESCE(total_out.qty_out,0) as qty_out,
//         COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
//         (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
//         COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
//         COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
//         from drugs
//         left join (select drug_ins.drug_id,
//         sum(case 
//                 when transactions.`status` ='out' then
//                 transactions.qty * -1
//                 else
//                 transactions.qty
//         end ) as stock_awal
//         from drug_ins
//         left join transactions on transactions.drug_receive_id = drug_ins.id
// 				join locators on drug_ins.locator_id = locators.id
//         where transactions.created_at < '".$_start_date."' and transactions.status not in ('retur','receive') and locators.name ='PUSKESMAS KELILING'
//         GROUP BY drug_ins.drug_id
//         ) stock_awal on drugs.id = stock_awal.drug_id
//         left join
//         (
//             select drug_ins.drug_id,
//             sum(qty_in) as qty_in
//             from drug_ins
// 						join locators on drug_ins.locator_id = locators.id
//             where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS KELILING'
//             GROUP BY drug_ins.drug_id
//         ) total_receive on drugs.id = total_receive.drug_id
//         left join (
//         select drug_ins.drug_id,
//         sum(transactions.qty) as qty_out
//         from transactions
//         join drug_ins on transactions.drug_in_id = drug_ins.id
// 										join locators on drug_ins.locator_id = locators.id
//         where transactions.`status` ='out' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS KELILING'
//         GROUP BY drug_ins.drug_id
//         ) total_out on drugs.id = total_out.drug_id ) puskesmas_keliling on puskesmas_induk.id = puskesmas_keliling.id
				
        
        
//         ");
        
//         //dd($jumlah_resep);
//         $file_name = 'REPORT_DISTRIBUSI'.'_FROM_'.$_start_date.'_TO_'.$_end_date;
    
//         return Excel::create($file_name,function($excel) use ($report_lplpo,$_start_date,$_end_date)
//         {
//             $excel->sheet('ACTIVE',function($sheet)use($report_lplpo,$_start_date,$_end_date)
//             {
//                 $sheet->mergeCells('A1:A2');
//                 $sheet->setCellValue('A1','NAMA OBAT');
//                 $sheet->mergeCells('B1:B2');
//                 $sheet->setCellValue('B1','SATUAN');
//                 $sheet->mergeCells('C1:C2');
//                 $sheet->setCellValue('C1','STOK AWAL PUSKESMAS INDUK');
//                 $sheet->mergeCells('D1:D2');
//                 $sheet->setCellValue('D1','PENERIMAAN PUSKESMAS INDUK');
//                 $sheet->mergeCells('E1:E2');
//                 $sheet->setCellValue('E1','PERSEDIAAN  PUSKESMAS INDUK');
//                 $sheet->mergeCells('F1:F2');
//                 $sheet->setCellValue('F1','PEMAKAIAN PUSKESMAS INDUK');
//                 $sheet->mergeCells('G1:G2');
//                 $sheet->setCellValue('G1','SISA STOK PUSKESMAS INDUK');
//                 $sheet->mergeCells('H1:H2');
//                 $sheet->setCellValue('H1','STOK OPTIMUM PUSKESMAS INDUK');
//                 $sheet->mergeCells('I1:I2');
//                 $sheet->setCellValue('I1','PERMINTAAN PUSKESMAS INDUK');
//                 $sheet->mergeCells('J1:J2');
//                 $sheet->setCellValue('J1','STOK AWAL PUSKESMAS PEMBANTU');
//                 $sheet->mergeCells('K1:K2');
//                 $sheet->setCellValue('K1','PENERIMAAN  PUSKESMAS PEMBANTU');
//                 $sheet->mergeCells('L1:L2');
//                 $sheet->setCellValue('L1','PERSEDIAAN PUSKESMAS PEMBANTU');
//                 $sheet->mergeCells('M1:M2');
//                 $sheet->setCellValue('M1','PEMAKAIAN PUSKESMAS PEMBANTU');
//                 $sheet->mergeCells('N1:N2');
//                 $sheet->setCellValue('N1','SISA STOK PUSKESMAS PEMBANTU');
//                 $sheet->mergeCells('O1:O2');
//                 $sheet->setCellValue('O1','STOK OPTIMUM  PUSKESMAS PEMBANTU');
//                 $sheet->mergeCells('P1:P2');
//                 $sheet->setCellValue('P1','PERMINTAAN PUSKESMAS PEMBANTU');
//                 $sheet->mergeCells('Q1:Q2');
//                 $sheet->setCellValue('Q1','STOK AWAL PUSKESMAS KELILING');
//                 $sheet->mergeCells('R1:R2');
//                 $sheet->setCellValue('R1','PENERIMAAN  PUSKESMAS KELILING');
//                 $sheet->mergeCells('S1:S2');
//                 $sheet->setCellValue('S1','PERSEDIAAN  PUSKESMAS KELILING');
//                 $sheet->mergeCells('T1:T2');
//                 $sheet->setCellValue('T1','PEMAKAIAN PUSKESMAS KELILING');
//                 $sheet->mergeCells('U1:U2');
//                 $sheet->setCellValue('U1','SISA STOK  PUSKESMAS KELILING');
//                 $sheet->mergeCells('V1:V2');
//                 $sheet->setCellValue('V1','STOK OPTIMUM  PUSKESMAS KELILING');
//                 $sheet->mergeCells('W1:W2');
//                 $sheet->setCellValue('W1','PERMINTAAN PUSKESMAS KELILING');

//                 $sheet->setAutoSize(true);
//                     $sheet->cell('A1:L1', function($cell) {
//                         $cell->setBackground('#808080');
//                         $cell->setValignment('center');
//                         $cell->setAlignment('center');
//                     });
//                     $sheet->cell('J2:L2', function($cell) {
//                         $cell->setBackground('#808080');
//                         $cell->setValignment('center');
//                         $cell->setAlignment('center');
//                     });


            
//             $row=3;

           
//             foreach ($report_lplpo as $i) 
//             {  
//                 $sheet->setCellValue('A'.$row,$i->name);
//                 $sheet->setCellValue('B'.$row,'');
//                 $sheet->setCellValue('C'.$row,$i->stock_awal);
//                 $sheet->setCellValue('D'.$row,$i->qty_in);
//                 $sheet->setCellValue('E'.$row,$i->persediaan);
//                 $sheet->setCellValue('F'.$row,$i->qty_out);
//                 $sheet->setCellValue('G'.$row,$i->sisa);
//                 $sheet->setCellValue('H'.$row,$i->stok_optimum);
//                 if($i->permintaan < 0)
//                 {
//                     $total_permintaan = 0;
//                 }
//                 else
//                 {
//                     $total_permintaan = $i->permintaan;
//                 }
//                 $sheet->setCellValue('I'.$row,$total_permintaan);
//                 //PEMBANTU
//                 $sheet->setCellValue('J'.$row,$i->pembantu_stock_awal);
//                 $sheet->setCellValue('K'.$row,$i->pembantu_qty_in);
//                 $sheet->setCellValue('L'.$row,$i->pembantu_persediaan);
//                 $sheet->setCellValue('M'.$row,$i->pembantu_qty_out);
//                 $sheet->setCellValue('N'.$row,$i->pembantu_sisa);
//                 $sheet->setCellValue('O'.$row,$i->pembantu_stok_optimum);
//                 if($i->pembantu_permintaan < 0)
//                 {
//                     $total_permintaan = 0;
//                 }
//                 else
//                 {
//                     $total_permintaan = $i->pembantu_permintaan;
//                 }
//                 $sheet->setCellValue('P'.$row,$total_permintaan);
//                 //KELILING
//                 $sheet->setCellValue('Q'.$row,$i->keliling_stock_awal);
//                 $sheet->setCellValue('R'.$row,$i->keliling_qty_in);
//                 $sheet->setCellValue('S'.$row,$i->keliling_persediaan);
//                 $sheet->setCellValue('T'.$row,$i->keliling_qty_out);
//                 $sheet->setCellValue('U'.$row,$i->keliling_sisa);
//                 $sheet->setCellValue('V'.$row,$i->keliling_stok_optimum);
//                 if($i->keliling_permintaan < 0)
//                 {
//                     $total_permintaan = 0;
//                 }
//                 else
//                 {
//                     $total_permintaan = $i->keliling_permintaan;
//                 }
//                 $sheet->setCellValue('W'.$row,$total_permintaan);
//                 // $sheet->setCellValue('J'.$row,$i->qty_apbd_kota);
//                 // $sheet->setCellValue('K'.$row,$i->qty_apbd_provinsi);
//                 // $sheet->setCellValue('L'.$row,$i->qty_dak);
//                 $row++;
//             }

//             $sheet->mergeCells('A'.($row+2).':C'.($row+2));
//             $sheet->setCellValue('A'.($row+2),'MENGETAHUI');
//             $sheet->mergeCells('A'.($row+3).':C'.($row+3));
//             $sheet->setCellValue('A'.($row+3),'a.n KEPALA DINAS KESEHATAN');
//             $sheet->mergeCells('A'.($row+4).':C'.($row+4));
//             $sheet->setCellValue('A'.($row+4),'KOTA SURAKARTA');
//             $sheet->mergeCells('A'.($row+5).':C'.($row+5));
//             $sheet->setCellValue('A'.($row+5),'KEPALA BIDANG DATA DAN SUMBER DAYA KESEHATAN');
//             $sheet->mergeCells('A'.($row+9).':C'.($row+9));
//             $sheet->setCellValue('A'.($row+9),'dr. SRI RAHAYU SUSILOWATI');
//             $sheet->mergeCells('A'.($row+10).':C'.($row+10));
//             $sheet->setCellValue('A'.($row+10),'Pembina');

//             $sheet->cell('A'.($row+2).':A'.($row+10), function($cell) {
//                 $cell->setAlignment('center');
//             });

//             $sheet->mergeCells('E'.($row+2).':G'.($row+2));
//             $sheet->setCellValue('E'.($row+2),'YANG MENYERAHKAN');
//             $sheet->mergeCells('E'.($row+3).':G'.($row+3));
//             $sheet->setCellValue('E'.($row+3),'KEPALA UPT INSTALASI FARMASI');
//             $sheet->mergeCells('E'.($row+4).':G'.($row+4));
//             $sheet->setCellValue('E'.($row+4),'DINAS KESEHATAN KOTA SURAKARTA');
//             $sheet->mergeCells('E'.($row+5).':G'.($row+5));
//             $sheet->setCellValue('E'.($row+5),'');
//             $sheet->mergeCells('E'.($row+9).':G'.($row+9));
//             $sheet->setCellValue('E'.($row+9),'HERU CAHYONO, S,Si.,Apt.');
//             $sheet->mergeCells('E'.($row+10).':G'.($row+10));
//             $sheet->setCellValue('E'.($row+10),'Pembina');

//             $sheet->cell('E'.($row+2).':E'.($row+10), function($cell) {
//                 $cell->setAlignment('center');
//             });

//             $sheet->mergeCells('I'.($row+2).':K'.($row+2));
//             $sheet->setCellValue('I'.($row+2),'YANG MELAPORKAN');
//             $sheet->mergeCells('I'.($row+3).':K'.($row+3));
//             $sheet->setCellValue('I'.($row+3),'KEPALA UPT PUSKESMAS JAYENGAN');
//             $sheet->mergeCells('I'.($row+4).':K'.($row+4));
//             $sheet->setCellValue('I'.($row+4),'DINAS KESEHATAN KOTA SURAKARTA');
//             $sheet->mergeCells('I'.($row+5).':K'.($row+5));
//             $sheet->setCellValue('I'.($row+5),'');
//             $sheet->mergeCells('I'.($row+9).':K'.($row+9));
//             $sheet->setCellValue('I'.($row+9),'dr. ANJANG KUSUMANETRA');
//             $sheet->mergeCells('I'.($row+10).':K'.($row+10));
//             $sheet->setCellValue('I'.($row+10),'Penata Tk.1');

//             $sheet->mergeCells('M'.($row+2).':O'.($row+2));
//             $sheet->setCellValue('M'.($row+2),'YANG MENERIMA');

//             $sheet->cell('I'.($row+2).':I'.($row+10), function($cell) {
//                 $cell->setAlignment('center');
                
//             });

//             $sheet->prependRow(1, array(
//                 ''
//             ));
//             $sheet->prependRow(1, array(
//                 ''
//             ));
//             $sheet->prependRow(1, array(
//                 ''
//             ));
//             $sheet->prependRow(1, array(
//                 ''
//             ));
//             $sheet->mergeCells('A1:K1');
//             $sheet->setCellValue('A1','LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT PER DISTRIBUSI');
//             $sheet->cell('A1:K1', function($cell) {
//                 $cell->setAlignment('center');
                
//             });

//             $sheet->mergeCells('A2:K2');
//             $sheet->setCellValue('A2','PUSKESMAS JAYENGAN');
//             $sheet->cell('A2:K2', function($cell) {
//                 $cell->setAlignment('center');
                
//             });

//             $resep = DB::select("select count(0) as jumlah from recipes where created_at >= '".$_start_date."' and created_at <= '".$_end_date."'");
//             $jumlah_resep = $resep[0];
            
//             $sheet->setCellValue('K4','Jumlah Resep : '.$jumlah_resep->jumlah);

//             });

//         })
//         ->export('xlsx');
//     }
public function export(Request $request)
{
            $_start_date = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d');
        $_end_date   = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d');
        $_start_date = $_start_date.' 00:00:00';
        $_end_date = $_end_date.' 23:59:59';

        $report_lplpo = DB::select("select stok_gudang.stock as stok_gudang,
        puskesmas_induk.*,
        (puskesmas_induk.sisa - COALESCE(retur_induk.qty_out,0)) as sisa_total,
        retur_induk.qty_in as induk_retur_in,
        retur_induk.qty_out as induk_retur_out,
        puskesmas_keliling.permintaan as keliling_permintaan,
        puskesmas_keliling.stock_awal as keliling_stock_awal,
        puskesmas_keliling.persediaan as keliling_persediaan,
        puskesmas_keliling.qty_in as keliling_qty_in,
        puskesmas_keliling.qty_out as keliling_qty_out,
        (puskesmas_keliling.sisa - COALESCE(retur_keliling.qty_out,0)) as keliling_sisa,
        puskesmas_keliling.stok_optimum as keliling_stok_optimum,
        retur_keliling.qty_in as keliling_retur_in,
        retur_keliling.qty_out as keliling_retur_out,
        
        puskesmas_pembantu.permintaan as pembantu_permintaan,
        puskesmas_pembantu.stock_awal as pembantu_stock_awal,
        puskesmas_pembantu.persediaan as pembantu_persediaan,
        puskesmas_pembantu.qty_in as pembantu_qty_in,
        puskesmas_pembantu.qty_out as pembantu_qty_out,
        (puskesmas_pembantu.sisa - COALESCE(retur_pembantu.qty_out,0)) as pembantu_sisa,
        puskesmas_pembantu.stok_optimum as pembantu_stok_optimum,
        retur_pembantu.qty_in as pembantu_retur_in,
        retur_pembantu.qty_out as pembantu_retur_out
        from (select drugs.id,
                drugs.`name`,
                COALESCE(stock_awal.stock_awal,0) as stock_awal,
                COALESCE(total_receive.qty_in,0) as qty_in,
                COALESCE(total_out.qty_out,0) as qty_out,
                COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
                (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
                COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
                COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
                from drugs
                left join (select drug_ins.drug_id,
                sum(case 
                        when transactions.`status` ='out' then
                        transactions.qty * -1
                        else
                        transactions.qty
                end ) as stock_awal
                from drug_ins
                left join transactions on transactions.drug_receive_id = drug_ins.id
                        join locators on drug_ins.locator_id = locators.id
                where transactions.created_at < '".$_start_date."' and transactions.status not in ('retur','receive') and locators.name ='PUSKESMAS INDUK'
                GROUP BY drug_ins.drug_id
                ) stock_awal on drugs.id = stock_awal.drug_id
                left join
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS INDUK'
                    GROUP BY drug_ins.drug_id
                ) total_receive on drugs.id = total_receive.drug_id
                left join (
                select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='out' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS INDUK'
                GROUP BY drug_ins.drug_id
                ) total_out on drugs.id = total_out.drug_id ) puskesmas_induk
                        left join 
                        (
                        select drugs.id,
                drugs.`name`,
                COALESCE(stock_awal.stock_awal,0) as stock_awal,
                COALESCE(total_receive.qty_in,0) as qty_in,
                COALESCE(total_out.qty_out,0) as qty_out,
                COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
                (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
                COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
                COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
                from drugs
                left join (select drug_ins.drug_id,
                sum(case 
                        when transactions.`status` ='out' then
                        transactions.qty * -1
                        else
                        transactions.qty
                end ) as stock_awal
                from drug_ins
                left join transactions on transactions.drug_receive_id = drug_ins.id
                        join locators on drug_ins.locator_id = locators.id
                where transactions.created_at < '".$_start_date."' and transactions.status not in ('retur','receive') and locators.name ='PUSKESMAS PEMBANTU'
                GROUP BY drug_ins.drug_id
                ) stock_awal on drugs.id = stock_awal.drug_id
                left join
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS PEMBANTU'
                    GROUP BY drug_ins.drug_id
                ) total_receive on drugs.id = total_receive.drug_id
                left join (
                select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='out' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS PEMBANTU'
                GROUP BY drug_ins.drug_id
                ) total_out on drugs.id = total_out.drug_id ) puskesmas_pembantu on puskesmas_induk.id = puskesmas_pembantu.id
                        left join(
                        select drugs.id,
                drugs.`name`,
                COALESCE(stock_awal.stock_awal,0) as stock_awal,
                COALESCE(total_receive.qty_in,0) as qty_in,
                COALESCE(total_out.qty_out,0) as qty_out,
                COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
                (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
                COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
                COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
                from drugs
                left join (select drug_ins.drug_id,
                sum(case 
                        when transactions.`status` ='out' then
                        transactions.qty * -1
                        else
                        transactions.qty
                end ) as stock_awal
                from drug_ins
                left join transactions on transactions.drug_receive_id = drug_ins.id
                        join locators on drug_ins.locator_id = locators.id
                where transactions.created_at < '".$_start_date."' and transactions.status not in ('retur','receive') and locators.name ='PUSKESMAS KELILING'
                GROUP BY drug_ins.drug_id
                ) stock_awal on drugs.id = stock_awal.drug_id
                left join
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS KELILING'
                    GROUP BY drug_ins.drug_id
                ) total_receive on drugs.id = total_receive.drug_id
                left join (
                select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='out' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS KELILING'
                GROUP BY drug_ins.drug_id
                ) total_out on drugs.id = total_out.drug_id ) puskesmas_keliling on puskesmas_induk.id = puskesmas_keliling.id
                        left join(
                        select drugs.id,
            sum(stock) as stock
            from drug_receives
            left join drugs on drug_receives.drug_id = drugs.id
              GROUP BY drugs.id,
            drugs.`name`
                        ) stok_gudang on puskesmas_induk.id = stok_gudang.id
                        left join (
                        select drugs.id,
        retur_in.qty_in,
        retur_out.qty_out
        from drugs left join 
                        (
                        select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on transactions.locator_id = locators.id
                where transactions.`status` ='retur' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS KELILING' and transactions.is_retur = true
                GROUP BY drug_ins.drug_id
                        ) retur_out on drugs.id = retur_out.drug_id
                     left join 
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS KELILING' and drug_ins.is_retur = true
                    GROUP BY drug_ins.drug_id
                ) retur_in  on drugs.id = retur_in.drug_id
                        ) retur_keliling on puskesmas_induk.id = retur_keliling.id
        left join (
                        select drugs.id,
        retur_in.qty_in,
        retur_out.qty_out
        from drugs left join 
                        (
                        select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on transactions.locator_id = locators.id
                where transactions.`status` ='retur' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS PEMBANTU' and transactions.is_retur = true
                GROUP BY drug_ins.drug_id
                        ) retur_out on drugs.id = retur_out.drug_id
                     left join 
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS PEMBANTU' and drug_ins.is_retur = true
                    GROUP BY drug_ins.drug_id
                ) retur_in  on drugs.id = retur_in.drug_id
                        ) retur_pembantu on puskesmas_induk.id = retur_pembantu.id
        left join (
                        select drugs.id,
        retur_in.qty_in,
        retur_out.qty_out
        from drugs left join 
                        (
                        select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on transactions.locator_id = locators.id
                where transactions.`status` ='retur' and transactions.created_at >= '".$_start_date."' and transactions.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS INDUK' and transactions.is_retur = true
                GROUP BY drug_ins.drug_id
                        ) retur_out on drugs.id = retur_out.drug_id
                     left join 
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '".$_start_date."' and drug_ins.created_at <= '".$_end_date."' and locators.name ='PUSKESMAS INDUK' and drug_ins.is_retur = true
                    GROUP BY drug_ins.drug_id
                ) retur_in  on drugs.id = retur_in.drug_id
                        ) retur_induk on puskesmas_induk.id = retur_induk.id
                        
                
        ");

        $file_name = 'REPORT_DISTRIBUSI'.'_FROM_'.$_start_date.'_TO_'.$_end_date;
        return Excel::create($file_name,function($excel) use ($report_lplpo,$_start_date,$_end_date)
        {
            $excel->sheet('ACTIVE',function($sheet)use($report_lplpo,$_start_date,$_end_date)
            {
                $sheet->mergeCells('A1:A2');
                $sheet->setCellValue('A1','NO');
                $sheet->mergeCells('B1:B2');
                $sheet->setCellValue('B1','NAMA OBAT');
                $sheet->mergeCells('C1:C2');
                $sheet->setCellValue('C1','GUDANG OBAT');
                $sheet->mergeCells('D1:G1');
                $sheet->setCellValue('D1','STOK AWAL');
                $sheet->setCellValue('D2','INDUK');
                $sheet->setCellValue('E2','PEMBANTU');
                $sheet->setCellValue('F2','PUSLING');
                $sheet->setCellValue('G2','TOTAL');
                $sheet->mergeCells('H1:K1');
                $sheet->setCellValue('H1','DISTRIBUSI');
                $sheet->setCellValue('H2','INDUK');
                $sheet->setCellValue('I2','PEMBANTU');
                $sheet->setCellValue('J2','PUSLING');
                $sheet->setCellValue('K2','TOTAL');
                $sheet->mergeCells('L1:O1');
                $sheet->setCellValue('L1','TERIMA RETUR');
                $sheet->setCellValue('L2','INDUK');
                $sheet->setCellValue('M2','PEMBANTU');
                $sheet->setCellValue('N2','PUSLING');
                $sheet->setCellValue('O2','TOTAL');
                $sheet->mergeCells('P1:S1');
                $sheet->setCellValue('P1','PENGELUARAN');
                $sheet->setCellValue('P2','INDUK');
                $sheet->setCellValue('Q2','PEMBANTU');
                $sheet->setCellValue('R2','PUSLING');
                $sheet->setCellValue('S2','TOTAL');
                $sheet->mergeCells('T1:W1');
                $sheet->setCellValue('T1','KELUAR RETUR');
                $sheet->setCellValue('T2','INDUK');
                $sheet->setCellValue('U2','PEMBANTU');
                $sheet->setCellValue('V2','PUSLING');
                $sheet->setCellValue('W2','TOTAL');
                $sheet->mergeCells('X1:AA1');
                $sheet->setCellValue('X1','STOCK AKHIR');
                $sheet->setCellValue('X2','INDUK');
                $sheet->setCellValue('Y2','PEMBANTU');
                $sheet->setCellValue('Z2','PUSLING');
                $sheet->setCellValue('AA2','TOTAL');
                
                $no =1;
                $row=3;
                //stok_gudang	id	name	stock_awal	qty_in	qty_out	persediaan	sisa	stok_optimum	permintaan	induk_retur_in	induk_retur_out	keliling_permintaan	keliling_stock_awal	keliling_persediaan	keliling_qty_in	keliling_qty_out	keliling_sisa	
                //keliling_stok_optimum	keliling_retur_in	keliling_retur_out	pembantu_permintaan	pembantu_stock_awal	pembantu_persediaan	pembantu_qty_in	pembantu_qty_out	pembantu_sisa	pembantu_stok_optimum	pembantu_retur_in	pembantu_retur_out


                foreach ($report_lplpo as $i) 
                {  
                    $sheet->setCellValue('A'.$row,$no);
                    $sheet->setCellValue('B'.$row,$i->name);
                    $sheet->setCellValue('C'.$row,$i->stok_gudang);
                    $sheet->setCellValue('D'.$row,$i->stock_awal);
                    $sheet->setCellValue('E'.$row,$i->keliling_stock_awal);
                    $sheet->setCellValue('F'.$row,$i->pembantu_stock_awal);
                    $total_stock_awal = $i->stock_awal + $i->keliling_stock_awal + $i->pembantu_stock_awal;
                    $sheet->setCellValue('G'.$row,$total_stock_awal);
                    //penerimaan
                    $sheet->setCellValue('H'.$row,$i->qty_in);
                    $sheet->setCellValue('I'.$row,$i->pembantu_qty_in);
                    $sheet->setCellValue('J'.$row,$i->keliling_qty_in);
                    $total_penerimaan = $i->qty_in + $i->keliling_qty_in + $i->pembantu_qty_in;
                    $sheet->setCellValue('K'.$row,$total_penerimaan);
                    //TERIMA RETUR
                    $sheet->setCellValue('L'.$row,$i->induk_retur_in);
                    $sheet->setCellValue('M'.$row,$i->pembantu_retur_in);
                    $sheet->setCellValue('N'.$row,$i->keliling_retur_in);
                    $total_retur = $i->induk_retur_in + $i->pembantu_retur_in + $i->keliling_retur_in;
                    $sheet->setCellValue('O'.$row,$total_retur);
                    //out
                    $sheet->setCellValue('P'.$row,$i->qty_out);
                    $sheet->setCellValue('Q'.$row,$i->pembantu_qty_out);
                    $sheet->setCellValue('R'.$row,$i->keliling_qty_out);
                    $total_out = $i->qty_out + $i->keliling_qty_out + $i->pembantu_qty_out;
                    $sheet->setCellValue('S'.$row,$total_out);
                    //OUT RETUR
                    $sheet->setCellValue('T'.$row,$i->induk_retur_out);
                    $sheet->setCellValue('U'.$row,$i->pembantu_retur_out);
                    $sheet->setCellValue('V'.$row,$i->keliling_retur_out);
                    $total_retur = $i->induk_retur_out + $i->keliling_retur_out + $i->pembantu_retur_out;
                    $sheet->setCellValue('W'.$row,$total_retur);
                    //
                    //sisa
                    $sheet->setCellValue('X'.$row,$i->sisa_total);
                    $sheet->setCellValue('Y'.$row,$i->pembantu_sisa);
                    $sheet->setCellValue('Z'.$row,$i->keliling_sisa);
                    $total_sisa = $i->sisa_total + $i->keliling_sisa + $i->pembantu_sisa;
                    $sheet->setCellValue('AA'.$row,$total_sisa);

                    $row++;
                    $no++;
                }

            });

        })
        ->export('xlsx');
    }

    
}
