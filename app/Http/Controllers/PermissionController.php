<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Permission;

class PermissionController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $breadcrumbActions = $this->breadcrumbAction();
        $msg = $request->session()->get('message');
        return view('permission.index',compact('msg','breadcrumbActions'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = Permission::orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('permission._action', [
                    'model' => $data,
                    'edit' => route('permission.edit',$data->id),
                    'delete' => route('permission.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }
    
    public function create(Request $request)
    {
        //if($request->session()->has('message')) $request->session()->forget('message');

        return view('permission.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Permission::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Nama sudah ada, silahkan cari nama permission lain.'], 422);


        try
        {
            DB::beginTransaction();
            Permission::firstorCreate([
                'name' => str_slug($request->name),
                'display_name' => $request->name,
                'description' => $request->description
            ]);
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function edit(Request $request,$id)
    {
        $permission = Permission::find($id);
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('permission.edit',compact('permission'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Permission::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        try
        {
            DB::beginTransaction();
            $permission = Permission::find($id);
            $permission->name = str_slug($request->name);
            $permission->display_name = $request->name;
            $permission->description = $request->description;
            $permission->save();
            
            DB::commit();
            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    public function destroy($id)
    {
        $permission = Permission::findorFail($id)->delete();
        return response()->json(200);
    }


    static function breadcrumbAction()
    {
        $settings           = array();

        $obj1               = new stdClass();
        $obj1->url          = route('permission.create');
        $obj1->iconClass    = 'icon-plus2';
        $obj1->name         = 'Create';
        $settings []        = $obj1;

        return $settings;
    }
}
