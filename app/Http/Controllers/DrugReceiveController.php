<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;
use App\Models\Locator;
use App\Models\Transaction;
use App\Models\DrugReceive;


class DrugReceiveController extends Controller
{
    public function index(Request $request)
    {
        $drugs = Drug::pluck('name', 'id')->all();
        //if($request->session()->has('message')) $request->session()->forget('message');
        return view('drug_receive.index',compact('drugs'));
    }

    public function create(Request $request)
    {
        $id_obat      = $request->name;
        $expired_date = Carbon::createFromFormat('d/m/Y', $request->expired)->format('Y-m-d');
        $note         = $request->note;
        $qty          = $request->receive_qty;
        $asal          = $request->asal;

        $obat         = Drug::find($id_obat);
        $nama         = $obat->name;
        $code         = $obat->code;
        $harga        = $request->price;
        $satuan       = $request->satuan;
        $total        = $qty * $harga;

        $array = [
            'drug_id'         => $id_obat,
            'nama'         => $nama,
            'asal'         => $asal,
            'code'         => $code,
            'expired_date' => $expired_date,
            'note'         => $note,
            'qty'          => $qty,
            'satuan'       => $satuan,
            'harga'        => $harga,
            'total'        => $total,

        ];

        //dd($array);

        return response()->json($array,200);

    }

    public function store(Request $request)
    {
        $list_receives           = json_decode($request->list_receive);
        
        try
        {
            DB::beginTransaction();

            foreach($list_receives as $key => $list_receive)
            {
                $drug_receive = DrugReceive::firstorCreate([
                    'drug_id'      => $list_receive->drug_id,
                    'user_id'      => Auth::user()->id,
                    'qty_receive'  => $list_receive->qty,
                    'stock'        => $list_receive->qty,
                    'reserved'     => 0,
                    'note'         => $list_receive->note,
                    'expired_date' => $list_receive->expired_date,
                    'satuan'       => $list_receive->satuan,
                    'price'        => $list_receive->harga,
                    'asal'        => $list_receive->asal,
                ]);

                $locator_id = Locator::where('name', 'RECEIVE')->first()->id;

                Transaction::firstorCreate([
                    'drug_receive_id' => $drug_receive->id,
                    'user_id'         => Auth::user()->id,
                    'qty'             => $list_receive->qty,
                    'status'          => 'receive',
                    'note'            => $list_receive->note,
                    'locator_id'      => $locator_id,
                ]);

            }
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
