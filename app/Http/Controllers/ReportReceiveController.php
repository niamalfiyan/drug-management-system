<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Carbon\Carbon;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;
use App\Models\Permission;
use App\Models\Doctor;
use App\Models\Recipe;
use App\Models\Locator;
use App\Models\Transaction;
use App\Models\DrugReceive;
use App\Models\DrugIn;


class ReportReceiveController extends Controller
{
    public function index(Request $request)
    {
        return view('report_receive.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $data = DB::table('report_receive')->whereBetween('created_at',[$start_date,$end_date])->get();
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('report_receive._action', [
                    'model' => $data,
                    'edit' => route('reportReceive.edit',$data->id),
                    'delete' => route('reportReceive.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $_start_date = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d');
        $_end_date   = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d');
        $_start_date = $_start_date.' 00:00:00';
        $_end_date = $_end_date.' 23:59:59';
        
        $report_receive =DB::table('report_receive')
                        ->where('created_at','>=',$_start_date)
                        ->where('created_at','<=',$_end_date)
                        ->get();        

        $file_name = 'REPORT_RECEIVE'.'_FROM_'.$_start_date.'_TO_'.$_end_date;
    
        return Excel::create($file_name,function($excel) use ($report_receive)
        {
            $excel->sheet('ACTIVE',function($sheet)use($report_receive)
            {
                $sheet->setCellValue('A1','TANGGAL RECEIVE');
                $sheet->setCellValue('B1','KODE OBAT');
                $sheet->setCellValue('C1','NAMA OBAT');
                $sheet->setCellValue('D1','JENIS');
                $sheet->setCellValue('E1','SATUAN');
                $sheet->setCellValue('F1','QTY TERIMA');
                $sheet->setCellValue('G1','EXPIRED DATE');
                $sheet->setCellValue('H1','PIC');
                $sheet->setCellValue('I1','NOTE');
            
            $row=2;

            foreach ($report_receive as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->created_at);
                $sheet->setCellValue('B'.$row,$i->code);
                $sheet->setCellValue('C'.$row,$i->name);
                $sheet->setCellValue('D'.$row,$i->type);
                $sheet->setCellValue('E'.$row,$i->satuan);
                $sheet->setCellValue('F'.$row,$i->qty_receive);
                $sheet->setCellValue('G'.$row,$i->expired_date);
                $sheet->setCellValue('H'.$row,$i->username);
                $sheet->setCellValue('i'.$row,$i->note);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }
    public function edit(Request $request,$id)
    {
        $permission = Permission::find('2');
        $drugs         = Drug::pluck('name', 'id')->all();
        $data_receive = DrugReceive::where('id', $id)->first();
        //dd($data_receive);
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('report_receive.edit', compact('drugs', 'data_receive', 'permission'));
    }

    public function destroy($id)
    {
        $permission = DrugReceive::findorFail($id)->delete();
        return response()->json(200);
    }

    public function update(Request $request, $id)
    {

        try
        {
            DB::beginTransaction();
            $drug_receive               = DrugReceive::find($id);
            $drug_receive->note         = $request->note;
            $drug_receive->expired_date = $request->expired_date;
            $drug_receive->satuan       = $request->satuan;
            $drug_receive->price        = $request->price;
            $drug_receive->asal         = $request->asal;
            $drug_receive->qty_receive  = $request->qty_receive;
            $qty_receive_new            = $request->qty_receive;
            $stock_new                  = $qty_receive_new - $drug_receive->reserved;
            $drug_receive->stock        = $stock_new;
            $drug_receive->save();

            $drug_in              = DrugIn::where('drug_receive_id', $id)
                                    ->update(['expired_date' => $request->expired_date]);

            DB::commit();
            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }


}
