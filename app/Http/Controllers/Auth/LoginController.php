<?php namespace App\Http\Controllers\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/home';

    protected $username;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->username = $this->findUsername();
    }

    public function findUsername()
    {
        $login = request()->input('login');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'nik';
        request()->merge([$fieldType => $login]);
        return $fieldType;
    }

    public function username()
    {
        return $this->username;
    }

    public function login(Request $request){
    	$this->validate($request, [
            'login' => 'required', 'password' => 'required',
        ]);

        $credentials = $this->credentials($request);
        if(isset($credentials['nik'])){
            $is_active = User::where('nik',$credentials['nik'])->whereNull('deleted_at')->exists();
            $count = User::where('nik',$credentials['nik'])->whereNull('deleted_at')->count();
        }else if(isset($credentials['email'])){
            $is_active = User::where('email',$credentials['email'])->whereNull('deleted_at')->exists();
            $count = User::where('email',$credentials['email'])->whereNull('deleted_at')->count();
        }

        if($is_active)
        {
            if($count == 1 || $count == 0){
                if (Auth::validate($credentials)) {
                    $user = Auth::getLastAttempted();
                    Auth::login($user, $request->has('remember'));
                    return redirect()->intended('/home');
                }
    
                if(isset($credentials['nik'])){
                    return redirect('/login')
                    ->withInput($request->only($credentials['nik'], 'remember'))
                    ->withErrors([
                        'nik' => 'Nik / Password is wrong',
                    ]);
                }else if(isset($credentials['email'])){
                    return redirect('/login')
                    ->withInput($request->only($credentials['email'], 'remember'))
                    ->withErrors([
                        'email' => 'Email / Password is wrong',
                    ]);
                }
    
                
            }else{
                return redirect('/login')
                ->withInput($request->only($credentials['nik'], 'remember'))
                ->withErrors([
                    'nik' => 'Nik has 2 users, please do login using email',
                ]);
            }
        }else
        {
            return redirect('/login')
                ->withInput($request->only($credentials['nik'], 'remember'))
                ->withErrors([
                    'nik' => 'User not found',
                ]);
        }
        
    }
}
