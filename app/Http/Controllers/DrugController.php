<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Drug;


class DrugController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $breadcrumbActions = $this->breadcrumbAction();
        $msg = $request->session()->get('message');
        return view('drug.index',compact('msg','breadcrumbActions'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = Drug::orderby('created_at','desc');
            return datatables()->of($data)
            ->editColumn('price',function($data){
            	return number_format($data->price, 2, ',', '.');
            })
            ->addColumn('action', function($data) {
                return view('drug._action', [
                    'model' => $data,
                    'edit' => route('drug.edit',$data->id),
                    'delete' => route('drug.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        //if($request->session()->has('message')) $request->session()->forget('message');

        return view('drug.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Drug::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Nama sudah ada, silahkan cari nama permission lain.'], 422);


        try
        {
            DB::beginTransaction();
            Drug::firstorCreate([
                'name' => $request->name,
                'type' => $request->type,
                'code' => $request->code,
                'price' => $request->price
            ]);
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function destroy($id)
    {
        $drug = Drug::findorFail($id)->delete();
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        $drug = Drug::find($id);
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('drug.edit',compact('drug'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Drug::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Obat sudah ada, silahkan cari nama obat lain.'], 422);

        try
        {
            DB::beginTransaction();
            $drug = Drug::find($id);
            $drug->name = str_slug($request->name);
            $drug->type = $request->type;
            $drug->code = $request->code;
            $drug->price = $request->price;
            $drug->save();
            
            DB::commit();
            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }


    static function breadcrumbAction()
    {
        $settings           = array();

        $obj1               = new stdClass();
        $obj1->url          = route('drug.create');
        $obj1->iconClass    = 'icon-plus2';
        $obj1->name         = 'Create';
        $settings []        = $obj1;

        return $settings;
    }
}
