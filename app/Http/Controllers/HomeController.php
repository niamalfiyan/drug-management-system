<?php namespace App\Http\Controllers;

use Hash;
use File;
use Auth;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {   
        $breadcrumbActions = $this->breadcrumbAction();
        return view('home',compact('breadcrumbActions'));
    }

    public function accountSetting()
    {
        return view('account_setting');
    }

    public function showAvatar($filename)
    {
        $path = Config::get('storage.avatar');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function updateAccount(request $request,$id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $old_password = $request->old_password;
        $password = $request->new_password;
        $retype_password = $request->retype_password;
        $user = User::find($id);
        $image = null;
        if($password)
        {
            if (!Hash::check($old_password, $user->password)) return redirect()->back()
            ->withErrors([
                'old_password' => 'Password tidak sama dengan yang saat ini',
            ]);

            if ($password != $retype_password) return redirect()->back()
            ->withErrors([
                'retype_password' => 'Password yang anda masukan tidak sama dengan password baru',
            ]);
        }

       
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                
                if(File::exists($avatar)) File::delete($old_file);
                
                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        if ($password) $user->password = bcrypt($password);
        if ($image) $user->photo = $image;
        $user->save();

        return redirect()->route('accountSetting'); //->withSuccess();
        
        
    }

    static function breadcrumbAction()
    {
        $settings = array();

        $obj1 = new stdClass();
        $obj1->url       = route('home');
        $obj1->iconClass = 'icon-user-lock';
        $obj1->name      = 'Account security';
        $settings []    = $obj1;

        return $settings;
    }
}
