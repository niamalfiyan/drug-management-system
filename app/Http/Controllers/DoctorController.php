<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\doctor;

class DoctorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $breadcrumbActions = $this->breadcrumbAction();
        $msg = $request->session()->get('message');
        return view('doctor.index',compact('msg','breadcrumbActions'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = Doctor::orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('doctor._action', [
                    'model' => $data,
                    'edit' => route('doctor.edit',$data->id),
                    'delete' => route('doctor.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }
    
    public function create(Request $request)
    {
        //if($request->session()->has('message')) $request->session()->forget('message');

        return view('doctor.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Doctor::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Nama sudah ada, silahkan cari nama doctor lain.'], 422);


        try
        {
            DB::beginTransaction();
            Doctor::firstorCreate([
                'name' => $request->name,
                'description' => $request->description
            ]);
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function edit(Request $request,$id)
    {
        $doctor = Doctor::find($id);
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('doctor.edit',compact('doctor'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(Doctor::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        try
        {
            DB::beginTransaction();
            $permission = Doctor    ::find($id);
            $permission->name = $request->name;
            $permission->description = $request->description;
            $permission->save();
            
            DB::commit();
            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    public function destroy($id)
    {
        $permission = Doctor::findorFail($id)->delete();
        return response()->json(200);
    }


    static function breadcrumbAction()
    {
        $settings           = array();

        $obj1               = new stdClass();
        $obj1->url          = route('doctor.create');
        $obj1->iconClass    = 'icon-plus2';
        $obj1->name         = 'Create';
        $settings []        = $obj1;

        return $settings;
    }
}
