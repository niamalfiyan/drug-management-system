<?php namespace App\Http\Controllers;

use DB;
use File;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Factory;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $breadcrumbActions = $this->breadcrumbAction();
        $msg = $request->session()->get('message');
        return view('user.index',compact('msg','breadcrumbActions'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            if(auth::user()->is_super_admin)
            {
                $data = db::select(db::raw("select users.id AS id
                ,users.name as name
                ,users.nik
                ,users.department
                ,users.sex
                ,users.created_at
                from users 
                where users.deleted_at is null
                order by users.created_at desc"));
            }else
            {
                $data = db::select(db::raw("select users.id AS id
                ,users.nik
                ,users.name as name
                ,users.department
                ,users.sex
                ,users.created_at
                from users 
                where users.deleted_at is null
                and nik != '11111111'
                order by users.created_at desc"));
            } 

            return datatables()->of($data)
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('department',function($data){
            	return ucwords($data->department);
            })
            ->editColumn('sex',function($data){
            	return ucwords($data->sex);
            })
            ->addColumn('action', function($data) {
                return view('user._action', [
                    'model' => $data,
                    'edit' => route('user.edit',$data->id),
                    'delete' => route('user.destroy',$data->id),
                    'reset' => route('user.resetPassword',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $is_super_admin = Auth::user()->is_super_admin;
        if($is_super_admin)
        {
            $roles = Role::pluck('display_name', 'id')->all();
        }else
        {
            $roles = Role::where('name','!=','super-admin')->pluck('display_name', 'id')->all();
        } 

        return view('user.create',compact('roles'));
    }

    public function store(Request $request)
    {
        // $avatar = Config::get('storage.avatar');
        // if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
            'sex' => 'required',
        ]);

        if($request->nik)
        {
            if(User::where('nik',$request->nik)->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        $image = null;
        // if ($request->hasFile('photo')) 
        // {
        //     if ($request->file('photo')->isValid()) 
        //     {
        //         $file = $request->file('photo');
        //         $now = Carbon::now()->format('u');
        //         $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        //         $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

        //         //Resize Function
        //         $image_resize = Image::make($file->getRealPath());
        //         $height = $image_resize->height();
        //         $width = $image_resize->width();

        //         $newWidth = 130;
        //         $newHeight = 130;
        //         $image_resize->resize($newWidth, $newHeight);
        //         $image_resize->save($avatar.'/'.$image);
        //     }
        // }
        
        try
        {
            DB::beginTransaction();
            
            $user = User::firstorCreate([
                'name' => strtolower($request->name),
                'nik' => $request->nik,
                'sex' => $request->sex,
                'department' => strtolower($request->department),
                'warehouse_id' => '',
                //'photo' => $image,
                'email' => $request->nik.'@'.'dummy.co.id',
                'email_verified_at' => carbon::now(),
                'password' => bcrypt('password1'),
                'is_super_admin' => ($request->has('is_super_admin'))? true : false,
                //'create_user_id' => auth::user()->id
            ]);

            $mappings =  json_decode($request->mappings);
            $array = array();

            foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];
            
            if($user->save()) $user->attachRoles($array);
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function storeRole(Request $request)
    {
        $user = User::find($request->user_id);
        $user->attachRoles([$request->role_id]);
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $is_super_admin = Auth::user()->is_super_admin;
        if($is_super_admin)
        {
            $roles      = Role::pluck('display_name', 'id')->all();
            $factory_id = null;
        }else
        {
            $roles      = Role::where('name','!=','super-admin')->pluck('display_name', 'id')->all();
            $factory_id = Auth::user()->factory_id;
        } 

        // $factories      = Factory::pluck('name', 'id')->all();
        $user           = User::find($id);
        $user_roles     = $user->roles()->get();
        $mappings       = array();

        foreach ($user_roles as $key => $user_role) 
        {
            $obj           = new stdClass;
            $obj->id       = $user_role->id;
            $obj->name     = $user_role->name;
            $mappings []   = $obj;
        }

        return view('user.edit',compact('roles','factory_id','user','mappings'));
    }

    public function dataRole(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select roles.id as id
            ,roles.display_name
            from roles 
            join role_user on role_user.role_id = roles.id 
            where role_user.user_id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('user.destroyRoleUser',[$id,($data)?$data->id : null]),
                ]);

               
            })
            ->make(true);
        }
        
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|min:3',
            'sex' => 'required',
        ]);
        
        if($request->nik)
        {
            if(User::where([
                ['nik',$request->nik],
                ['id','!=',$id],
            ])->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        $user = User::find($id);
        
        $user->nik = $request->nik;
        $user->name = strtolower($request->name);
        $user->sex = $request->sex;
        $user->department = strtolower($request->department);
        //if ($request->password) $user->password = bcrypt($request->password);
        $user->save();

        $request->session()->flash('message', 'success_2');
        return response()->json(200);
    }

    public function destroy($id)
    {
        $user = User::findorFail($id);
        $user->deleted_at = carbon::now();
        //$user->delete_user_id = auth::user()->id;
        $user->save();
        return response()->json(200);
    }

    public function resetPassword($id)
    {
        $user = User::findorFail($id);
        $user->password = bcrypt('password1');
        $user->save();
        return response()->json(200);
    }

    public function destroyRoleUser($user_id,$role_id)
    {
        $user = User::find($user_id);
        $roles = $user->roles()->where('role_id','!=',$role_id)->get();
        $array = array();
        foreach ($roles as $key => $role) {
            $array [] = $role->id;
        }
        
        $user->roles()->sync([]);
        $user->attachRoles($array);

        return response()->json($roles);
    }

    static function breadcrumbAction()
    {
        $settings           = array();

        $obj1               = new stdClass();
        $obj1->url          = route('user.create');
        $obj1->iconClass    = 'icon-plus2';
        $obj1->name         = 'Create';
        $settings []        = $obj1;

        return $settings;
    }
}
