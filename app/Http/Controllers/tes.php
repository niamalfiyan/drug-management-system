select stok_gudang.stock as stok_gudang,
        puskesmas_induk.*,
        retur_induk.qty_in as induk_retur_in,
        retur_induk.qty_out as induk_retur_out,
        puskesmas_keliling.permintaan as keliling_permintaan,
        puskesmas_keliling.stock_awal as keliling_stock_awal,
        puskesmas_keliling.persediaan as keliling_persediaan,
        puskesmas_keliling.qty_in as keliling_qty_in,
        puskesmas_keliling.qty_out as keliling_qty_out,
        puskesmas_keliling.sisa as keliling_sisa,
        puskesmas_keliling.stok_optimum as keliling_stok_optimum,
        retur_keliling.qty_in as keliling_retur_in,
        retur_keliling.qty_out as keliling_retur_out,
        
        puskesmas_pembantu.permintaan as pembantu_permintaan,
        puskesmas_pembantu.stock_awal as pembantu_stock_awal,
        puskesmas_pembantu.persediaan as pembantu_persediaan,
        puskesmas_pembantu.qty_in as pembantu_qty_in,
        puskesmas_pembantu.qty_out as pembantu_qty_out,
        puskesmas_pembantu.sisa as pembantu_sisa,
        puskesmas_pembantu.stok_optimum as pembantu_stok_optimum,
        retur_pembantu.qty_in as pembantu_retur_in,
        retur_pembantu.qty_out as pembantu_retur_out
        from (select drugs.id,
                drugs.`name`,
                COALESCE(stock_awal.stock_awal,0) as stock_awal,
                COALESCE(total_receive.qty_in,0) as qty_in,
                COALESCE(total_out.qty_out,0) as qty_out,
                COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
                (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
                COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
                COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
                from drugs
                left join (select drug_ins.drug_id,
                sum(case 
                        when transactions.`status` ='out' then
                        transactions.qty * -1
                        else
                        transactions.qty
                end ) as stock_awal
                from drug_ins
                left join transactions on transactions.drug_receive_id = drug_ins.id
                        join locators on drug_ins.locator_id = locators.id
                where transactions.created_at < '2021-11-01' and transactions.status != 'receive' and locators.name ='PUSKESMAS INDUK'
                GROUP BY drug_ins.drug_id
                ) stock_awal on drugs.id = stock_awal.drug_id
                left join
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '2021-11-01' and drug_ins.created_at <= '2021-12-01' and locators.name ='PUSKESMAS INDUK'
                    GROUP BY drug_ins.drug_id
                ) total_receive on drugs.id = total_receive.drug_id
                left join (
                select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='out' and transactions.created_at >= '2021-11-01' and transactions.created_at <= '2021-12-01' and locators.name ='PUSKESMAS INDUK'
                GROUP BY drug_ins.drug_id
                ) total_out on drugs.id = total_out.drug_id ) puskesmas_induk
                        left join 
                        (
                        select drugs.id,
                drugs.`name`,
                COALESCE(stock_awal.stock_awal,0) as stock_awal,
                COALESCE(total_receive.qty_in,0) as qty_in,
                COALESCE(total_out.qty_out,0) as qty_out,
                COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
                (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
                COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
                COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
                from drugs
                left join (select drug_ins.drug_id,
                sum(case 
                        when transactions.`status` ='out' then
                        transactions.qty * -1
                        else
                        transactions.qty
                end ) as stock_awal
                from drug_ins
                left join transactions on transactions.drug_receive_id = drug_ins.id
                        join locators on drug_ins.locator_id = locators.id
                where transactions.created_at < '2021-11-01' and transactions.status != 'receive' and locators.name ='PUSKESMAS PEMBANTU'
                GROUP BY drug_ins.drug_id
                ) stock_awal on drugs.id = stock_awal.drug_id
                left join
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '2021-11-01' and drug_ins.created_at <= '2021-12-01' and locators.name ='PUSKESMAS PEMBANTU'
                    GROUP BY drug_ins.drug_id
                ) total_receive on drugs.id = total_receive.drug_id
                left join (
                select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='out' and transactions.created_at >= '2021-11-01' and transactions.created_at <= '2021-12-01' and locators.name ='PUSKESMAS PEMBANTU'
                GROUP BY drug_ins.drug_id
                ) total_out on drugs.id = total_out.drug_id ) puskesmas_pembantu on puskesmas_induk.id = puskesmas_pembantu.id
                        left join(
                        select drugs.id,
                drugs.`name`,
                COALESCE(stock_awal.stock_awal,0) as stock_awal,
                COALESCE(total_receive.qty_in,0) as qty_in,
                COALESCE(total_out.qty_out,0) as qty_out,
                COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) as persediaan,
                (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as sisa,
                COALESCE(total_out.qty_out,0) * 2.5 as stok_optimum,
                COALESCE(total_out.qty_out,0) * 2.5 - (COALESCE(stock_awal.stock_awal,0) + COALESCE(total_receive.qty_in,0) - COALESCE(total_out.qty_out,0)) as permintaan
                from drugs
                left join (select drug_ins.drug_id,
                sum(case 
                        when transactions.`status` ='out' then
                        transactions.qty * -1
                        else
                        transactions.qty
                end ) as stock_awal
                from drug_ins
                left join transactions on transactions.drug_receive_id = drug_ins.id
                        join locators on drug_ins.locator_id = locators.id
                where transactions.created_at < '2021-11-01' and transactions.status != 'receive' and locators.name ='PUSKESMAS KELILING'
                GROUP BY drug_ins.drug_id
                ) stock_awal on drugs.id = stock_awal.drug_id
                left join
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '2021-11-01' and drug_ins.created_at <= '2021-12-01' and locators.name ='PUSKESMAS KELILING'
                    GROUP BY drug_ins.drug_id
                ) total_receive on drugs.id = total_receive.drug_id
                left join (
                select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='out' and transactions.created_at >= '2021-11-01' and transactions.created_at <= '2021-12-01' and locators.name ='PUSKESMAS KELILING'
                GROUP BY drug_ins.drug_id
                ) total_out on drugs.id = total_out.drug_id ) puskesmas_keliling on puskesmas_induk.id = puskesmas_keliling.id
                        left join(
                        select drugs.id,
            sum(stock) as stock
            from drug_receives
            left join drugs on drug_receives.drug_id = drugs.id
              GROUP BY drugs.id,
            drugs.`name`
                        ) stok_gudang on puskesmas_induk.id = stok_gudang.id
                        left join (
                        select drugs.id,
        retur_in.qty_in,
        retur_out.qty_out
        from drugs left join 
                        (
                        select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='retur' and transactions.created_at >= '2021-11-01' and transactions.created_at <= '2021-12-01' and locators.name ='PUSKESMAS KELILING' and transactions.is_retur = true
                GROUP BY drug_ins.drug_id
                        ) retur_out on drugs.id = retur_out.drug_id
                     left join 
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '2021-11-01' and drug_ins.created_at <= '2021-12-01' and locators.name ='PUSKESMAS KELILING' and drug_ins.is_retur = true
                    GROUP BY drug_ins.drug_id
                ) retur_in  on drugs.id = retur_in.drug_id
                        ) retur_keliling on puskesmas_induk.id = retur_keliling.id
        left join (
                        select drugs.id,
        retur_in.qty_in,
        retur_out.qty_out
        from drugs left join 
                        (
                        select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='retur' and transactions.created_at >= '2021-11-01' and transactions.created_at <= '2021-12-01' and locators.name ='PUSKESMAS PEMBANTU' and transactions.is_retur = true
                GROUP BY drug_ins.drug_id
                        ) retur_out on drugs.id = retur_out.drug_id
                     left join 
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '2021-11-01' and drug_ins.created_at <= '2021-12-01' and locators.name ='PUSKESMAS PEMBANTU' and drug_ins.is_retur = true
                    GROUP BY drug_ins.drug_id
                ) retur_in  on drugs.id = retur_in.drug_id
                        ) retur_pembantu on puskesmas_induk.id = retur_pembantu.id
        left join (
                        select drugs.id,
        retur_in.qty_in,
        retur_out.qty_out
        from drugs left join 
                        (
                        select drug_ins.drug_id,
                sum(transactions.qty) as qty_out
                from transactions
                join drug_ins on transactions.drug_in_id = drug_ins.id
                                                join locators on drug_ins.locator_id = locators.id
                where transactions.`status` ='retur' and transactions.created_at >= '2021-11-01' and transactions.created_at <= '2021-12-01' and locators.name ='PUSKESMAS INDUK' and transactions.is_retur = true
                GROUP BY drug_ins.drug_id
                        ) retur_out on drugs.id = retur_out.drug_id
                     left join 
                (
                    select drug_ins.drug_id,
                    sum(qty_in) as qty_in
                    from drug_ins
                                join locators on drug_ins.locator_id = locators.id
                    where drug_ins.created_at >= '2021-11-01' and drug_ins.created_at <= '2021-12-01' and locators.name ='PUSKESMAS INDUK' and drug_ins.is_retur = true
                    GROUP BY drug_ins.drug_id
                ) retur_in  on drugs.id = retur_in.drug_id
                        ) retur_induk on puskesmas_induk.id = retur_induk.id