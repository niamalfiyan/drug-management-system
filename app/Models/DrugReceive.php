<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use Illuminate\Database\Eloquent\Model;

class DrugReceive extends Model
{
    protected $fillable = ['drug_id','stock','qty_receive','reserved','deleted_at','price','note','satuan','user_id','asal','expired_date'];
    protected $dates = ['created_at'];

    public function Drug(){
        return $this->belongsTo('App\Models\Drug');
    }

    public function users(){
        return $this->belongsTo('App\Models\User');
    }
}
