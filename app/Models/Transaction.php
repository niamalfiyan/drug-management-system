<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['drug_receive_id','qty','status','user_id','deleted_at','locator_id','note', 'recipe_id','racikan', 'dosis','drug_in_id','is_retur'];
    protected $dates = ['created_at'];

    public function drug_receives(){
        return $this->belongsTo('App\Models\DrugReceive');
    }

    public function users(){
        return $this->belongsTo('App\Models\User');
    }
}
