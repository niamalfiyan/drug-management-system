<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustPermission;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class Permission  extends EntrustPermission
{
    protected $fillable = ['name','display_name','description'];
    protected $dates = ['created_at'];
    use EntrustUserTrait;
    
    public function roles(){
        return $this->belongsToMany('App\Models\Role','permission_role','permission_id','role_id');
    }
}
