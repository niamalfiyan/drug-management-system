<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use Illuminate\Database\Eloquent\Model;


class Drug extends Model
{
    protected $fillable = ['name','code','type','price','deleted_at'];
    protected $dates = ['created_at'];

    public function drug_receives(){
        return $this->belongsToMany('App\Models\Drug','receive_drug','receive_id','drug_id');
    }
}
