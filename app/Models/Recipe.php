<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = ['nama_pasien','alamat','jenis_kelamin','diagnosis','berat_badan','tekanan_darah','status_jaminan',
    'tempat_pemeriksaan','tanggal_resep','tanggal_lahir','user_id','doctor_id','deleted_at'];
    protected $dates = ['created_at'];

    public function users(){
        return $this->belongsTo('App\Models\User');
    }
    public function Doctor(){
        return $this->belongsTo('App\Models\Doctor', 'doctor_id', 'id');
    }
}
