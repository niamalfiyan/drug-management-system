<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = ['name','description','deleted_at'];
    protected $dates = ['created_at'];
}
