<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use Illuminate\Database\Eloquent\Model;

class DrugIn extends Model
{
    protected $fillable = ['drug_receive_id','stock','qty_in','reserved','deleted_at','note','user_id','locator_id','is_retur', 'drug_id','expired_date'];
    protected $dates = ['created_at'];

    public function Drug(){
        return $this->belongsTo('App\Models\Drug');
    }

    public function users(){
        return $this->belongsTo('App\Models\User');
    }
}
