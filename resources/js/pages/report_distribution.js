$(function() {
    const dtableConfig = $('#dtable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        scrollY: 300,
        scroller: true,
        destroy: true,
        deferRender: true,
        bFilter: true,
        pageLength: 25,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/report/distribution/data',
        },
        fnCreatedRow: function(row, data, index) {
            var info = dtableConfig.page.info();
            var value = index + 1 + info.start + ' ' + data.action;
            $('td', row).eq(0).html(value);
        },
        columns: [
            { data: null, sortable: false, orderable: false, searchable: false },
            { data: 'id', name: 'id', searchable: true, visible: false, orderable: false },
            { data: 'nama_locator', name: 'nama_locator', searchable: true},
            { data: 'code', name: 'code', searchable: true, orderable: true },
            { data: 'name', name: 'name', searchable: true, orderable: true },
            { data: 'stock', name: 'stock', searchable: true, orderable: true },
        ]
    });

    const dtable = $('#dtable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();
});
