$(function() {
    const dtableConfig = $('#dtable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        scrollY: 300,
        scroller: true,
        destroy: true,
        deferRender: true,
        bFilter: true,
        pageLength: 25,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/report/out/data',
            data: function(d) {
                return $.extend({}, d, {
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        fnCreatedRow: function(row, data, index) {
            var info = dtableConfig.page.info();
            var value = index + 1 + info.start + ' ' + data.action;
            $('td', row).eq(0).html(value);
        },
        columns: [
            { data: null, sortable: false, orderable: false, searchable: false },
            { data: 'id', name: 'id', searchable: true, visible: false, orderable: false },
            { data: 'created_at', name: 'created_at', searchable: true, orderable: true },
            { data: 'nama_pasien', name: 'nama_pasien', searchable: true, orderable: true },
            { data: 'code', name: 'code', searchable: true, orderable: true },
            { data: 'name', name: 'name', searchable: true, orderable: true },
            { data: 'type', name: 'type', searchable: true, orderable: true },
            { data: 'satuan', name: 'satuan', searchable: true, orderable: true },
            { data: 'qty', name: 'qty', searchable: true, orderable: true },
            { data: 'expired_date', name: 'expired_date', searchable: true, orderable: true },
            { data: 'username', name: 'username', searchable: true, orderable: true },
        ]
    });

    const dtable = $('#dtable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });
});

function hapus(url) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    bootbox.confirm("Are you sure want to delete this data ?.", function(result) {
        if (result) {
            $.ajax({
                type: "delete",
                url: url,
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function() {
                    $.unblockUI();
                },
                error: function() {
                    $.unblockUI();
                }
            }).done(function($result) {
                $('#dtable').DataTable().ajax.reload();
                $("#alert_success").trigger("click", 'Data successfully deleted');
            });
        }
    });


}
