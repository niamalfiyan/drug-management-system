$(function() {
    const msg = $('#msg').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');


    mappings = JSON.parse($('#mappings').val());
    const form_status = $('#form_status').val();
    if (form_status == 'index') {
        console.log('masuk');
        const dtableConfig = $('#dtable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            scrollY: 300,
            scroller: true,
            destroy: true,
            deferRender: true,
            bFilter: true,
            pageLength: 25,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/account-management/role/data',
            },
            fnCreatedRow: function(row, data, index) {
                let info = dtableConfig.page.info();
                let value = index + 1 + info.start + ' ' + data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                { data: null, sortable: false, orderable: false, searchable: false },
                { data: 'id', name: 'id', searchable: true, visible: false, orderable: false },
                { data: 'display_name', name: 'display_name', searchable: true, orderable: true },
                { data: 'description', name: 'description', searchable: true, orderable: true },
            ]
        });

        const dtable = $('#dtable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function(e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        dtable.draw();
    } else if (form_status == 'create') {
        render();
    } else {
        const url_data_permission = $('#url_data_permission').val();

        $('#permissionTable').DataTable().destroy();
        $('#permissionTable tbody').empty();
        const table = $('#permissionTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 10,
            scrollY: 250,
            scroller: true,
            destroy: true,
            deferRender: true,
            bFilter: true,
            ajax: {
                type: 'GET',
                url: url_data_permission,
            },
            fnCreatedRow: function(row, data, index) {
                var info = table.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                { data: null, sortable: false, orderable: false, searchable: false },
                { data: 'display_name', name: 'display_name', searchable: true, orderable: true },
                { data: 'action', name: 'action', searchable: false, orderable: false },
            ]
        });

        var dtable2 = $('#permissionTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function(e) { // Bind our desired behavior
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable2.search(this.value).draw();
                }
                if (this.value == "") {
                    dtable2.search("").draw();
                }
                return;
            });
        dtable2.draw();

        render();
    }
});

$('#form').on('submit', function(event) {
    event.preventDefault();
    const name = $('#name').val();

    if (!name) {
        $("#alert_warning").trigger("click", 'Nama wajib diisi');
        return false
    }

    bootbox.confirm("Are you sure want to save this data ?.", function(result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#form').attr('action'),
                data: $('#form').serialize(),
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function() {
                    document.location.href = '/account-management/role';
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

                }
            });
        }
    });
});

$('#select_permission').on('change', function() {
    const value = $(this).val();
    const name = $(this).select2('data')[0].text;
    const form_status = $('#form_status').val();
    var type        = $('#type').val();

    if (value) {
        const input = {
            'id': value,
            'name': name,
            'type': type
        };

        let diff = checkItem(value);
        if (!diff) {
            $("#alert_warning").trigger("click", `${name} already selected`);
            $(this).val('').trigger('change');
            return false;
        }

        if (form_status == 'edit') {
            var role_id = $('#role_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                    type: 'post',
                    url: '/account-management/role/store/permission',
                    data: {
                        role_id: role_id,
                        permission_id: value
                    }
                })
                .done(function() {
                    $('#permissionTable').DataTable().ajax.reload();
                });
        }

        if (name) mappings.push(input);

        render();
        $(this).val('').trigger('change');
    }
});

function hapus(url) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            type: "delete",
            url: url,
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function() {
                $.unblockUI();
            },
            error: function() {
                $.unblockUI();
            }
        })
        .done(function() {
            $('#dtable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Data successfully deleted');
        });
}

function hapusModal(url) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            type: "post",
            url: url,
            beforeSend: function() {
                mappings = [];
            },
            success: function(response) {
                const permissions = response;
                for (idx in permissions) {
                    const data = permissions[idx];
                    let input = {
                        'id': data.id,
                        'name': data.display_name
                    };
                    mappings.push(input);
                }


            }
        })
        .done(function() {
            $('#permissionTable').DataTable().ajax.reload();
            render();
        });
}

function render() {
    getIndex();
    $('#mappings').val(JSON.stringify(mappings));

    const tmpl = $('#permission_table').html();
    Mustache.parse(tmpl);
    const data = { item: mappings };
    const html = Mustache.render(tmpl, data);
    $('#role_permission').html(html);
    bind();
}

function bind() {
    $('.btn-delete-item').on('click', deleteItem);
}

function getIndex() {
    for (idx in mappings) {
        mappings[idx]['_id'] = idx;
        mappings[idx]['no'] = parseInt(idx) + 1;
    }
}

function deleteItem() {
    const i = parseInt($(this).data('id'), 10);

    mappings.splice(i, 1);
    render();
}

function checkItem(id) {
    for (let i in mappings) {
        const data = mappings[i];

        if (data.id == id)
            return false;
    }

    return true;
}

