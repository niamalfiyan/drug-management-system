list_receive = JSON.parse($('#list_receive').val());
$(function () 
{
$('#form').on('submit', function(event) {
    event.preventDefault();
            $.ajax({
                type: "GET",
                url: $('#form').attr('action'),
                data: $('#form').serialize(),
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    console.log(response);
                    list_receive.push(response);
                    
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON);

                }
            })
            .done(function (response) 
            {
                render();
            });
});

$('#form_store').on('submit', function(event) {
    event.preventDefault();
    bootbox.confirm("Are you sure want to save this data ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#form_store').attr('action'),
                data: $('#form_store').serialize(),
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    console.log(response);
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON);

                }
            })
            .done(function (response) 
            {
                $('#list_receive').val('');
                $('#receive_qty').val('');
                $('#note').val('');
                list_receive = [];
				render();
                $("#alert_success").trigger("click", 'Successfully');
            });
        }
        });
    });
});

function render() 
{
    getIndex();
    $('#list_receive').val(JSON.stringify(list_receive));
    var tmpl = $('#drug_receive_table').html();
    Mustache.parse(tmpl);
    var data = { item: list_receive };
    var html = Mustache.render(tmpl, data);
    $('#tbody_drug_receive').html(html);
    bind();
}

function getIndex()
{
	for (idx in list_receive) 
	{
		list_receive[idx]['_idx'] 	= idx;
		list_receive[idx]['no'] 		= parseInt(idx) + 1;
	}
}



function bind()
{
	$('.btn-delete-item').on('click', deleteItem);
}



function deleteItem()
{
	var i = $(this).data('id');

	list_receive.splice(i, 1);
	render();
}

