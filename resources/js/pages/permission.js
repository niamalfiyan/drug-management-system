$(function() {

    const msg = $('#msg').val();

    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');


    const dtableConfig = $('#dtable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        scrollY: 300,
        scroller: true,
        destroy: true,
        deferRender: true,
        bFilter: true,
        pageLength: 25,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/account-management/permission/data',
        },
        fnCreatedRow: function(row, data, index) {
            var info = dtableConfig.page.info();
            var value = index + 1 + info.start + ' ' + data.action;
            $('td', row).eq(0).html(value);
        },
        columns: [
            { data: null, sortable: false, orderable: false, searchable: false },
            { data: 'id', name: 'id', searchable: true, visible: false, orderable: false },
            { data: 'display_name', name: 'display_name', searchable: true, orderable: true },
            { data: 'description', name: 'description', searchable: true, orderable: true },
        ]
    });

    const dtable = $('#dtable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();
});

$('#form').on('submit', function(event) {
    event.preventDefault();
    const name = $('#name').val();

    if (!name) {
        $("#alert_warning").trigger("click", 'Please insert name first');
        return false;
    }

    bootbox.confirm("Are you sure want to save this data ?.", function(result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#form').attr('action'),
                data: $('#form').serialize(),
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function() {
                    document.location.href = '/account-management/permission';
                },
                error: function(response) {
                    $.unblockUI();

                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                }
            });
        }
    });
});


function hapus(url) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    bootbox.confirm("Are you sure want to delete this data ?.", function(result) {
        if (result) {
            $.ajax({
                type: "delete",
                url: url,
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function() {
                    $.unblockUI();
                },
                error: function() {
                    $.unblockUI();
                }
            }).done(function($result) {
                $('#dtable').DataTable().ajax.reload();
                $("#alert_success").trigger("click", 'Data successfully deleted');
            });
        }
    });


}