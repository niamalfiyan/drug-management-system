<div class="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">
    <div class="sidebar-mobile-toggler text-center">
         <a href="#" class="sidebar-mobile-main-toggle">
            
         </a>
         Navigation
         <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-close2"></i>
         </a>
     </div>
     <div class="sidebar-content">
       <div class="sidebar-user-material">
             <div class="sidebar-user-material-body">
                 <div class="card-body text-center">
                     <a href="#">
                         @if(auth::user()->photo)
                             <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="img-fluid rounded-circle shadow-1 mb-3" alt="profile_photo" id="avatar_image" width="80" height="80">
                         @else
                             @if(strtoupper(auth::user()->sex) == 'LAKI')
                                 <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-fluid rounded-circle shadow-1 mb-3" alt="avatar_male" width="80" height="80" ></a>
                             @else
                                 <a href="#"><img src="{{ asset('images/female_avatar.png') }}" class="img-fluid rounded-circle shadow-1 mb-3" alt="avatar_female" width="80" height="80" ></a>
                             @endif
                         @endif
                     </a>
                     <h6 class="mb-0 text-white text-shadow-dark">{{ ucwords(Auth::user()->name) }}</h6>
                     <span class="font-size-sm text-white text-shadow-dark">{{ Auth::user()->nik }}</span>
                 </div>
                                             
                 <div class="sidebar-user-material-footer">
                     <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>My Account</span></a>
                 </div>
             </div>
 
             <div class="collapse" id="user-nav" style="">
                <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="{{ route('accountSetting') }}" class="nav-link {{ $active == 'account_setting' ? 'active' : '' }} legitRipple">
                            <i class="icon-cog5"></i>
                            <span>Account Settings</span>
                        </a>
                    </li>

                    
                    <li class="nav-item">
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             {{ csrf_field() }}
                         </form>

                        <a href="{{ route('logout') }}" class="nav-link legitRipple"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="icon-switch2"></i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>

             
         </div>
 
 
         <div class="card card-sidebar-mobile">
             <ul class="nav nav-sidebar" data-nav-type="accordion">
 
                 <!-- Main -->
                 <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                 <li class="nav-item">
                     <a href="{{ route('home') }}" class="nav-link {{ $active == 'dashboard' ? 'active' : '' }} legitRipple">
                         <i class="icon-home4"></i>
                         <span>
                             Dashboard
                         </span>
                     </a>
                 </li>
                 
                 @permission(['menu-user','menu-role','menu-permission', 'menu-obat', 'menu-drug', 'menu-doctor'])
                     <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Master Data</div> <i class="icon-menu" title="Forms"></i></li>
                 
                     @permission(['menu-user','menu-role','menu-permission'])
                        <li class="nav-item nav-item-submenu {{ in_array($active,['permission','role','user']) ? 'nav-item-expanded nav-item-close' : '' }}">
                            <a href="#" class="nav-link {{ in_array($active,['permission','role','user']) ? 'active' : '' }}"><i class="icon-users"></i> <span>Account Management</span></a>
        
                            <ul class="nav nav-group-sub" data-submenu-title="Account Management">
                                @permission('menu-permission')
                                    <li class="nav-item"><a href="{{ route('permission.index') }}" class="nav-link {{ $active == 'permission' ? 'active' : '' }} ">Permission</a></li>
                                @endpermission
                                @permission('menu-role')
                                    <li class="nav-item"><a href="{{ route('role.index') }}" class="nav-link {{ $active == 'role' ? 'active' : '' }} ">Role</a></li>
                                @endpermission
                                @permission('menu-user')
                                    <li class="nav-item"><a href="{{ route('user.index') }}" class="nav-link {{ $active == 'user' ? 'active' : '' }} ">User</a></li>
                                @endpermission


                            </ul>
                        </li>
                    @endpermission
                    @permission(['menu-drug'])
                        <li class="nav-item"><a href="{{ route('drug.index') }}" class="nav-link {{ $active == 'drug' ? 'active' : '' }} legitRipple"><i class="icon-plus2"></i><span>Obat</span></a></li>
                    @endpermission
                    @permission(['menu-doctor'])
                        <li class="nav-item"><a href="{{ route('doctor.index') }}" class="nav-link {{ $active == 'doctor' ? 'active' : '' }} legitRipple"><i class="icon-man-woman"></i><span>Dokter</span></a></li>
                    @endpermission
                 @endpermission
                 @permission(['menu-drug-receive','menu-drug-distribution','menu-drug-out'])
                 <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Transaction</div> <i class="icon-menu" title="Main"></i></li>
                    @permission(['menu-drug-receive'])
                        <li class="nav-item"><a href="{{ route('drugReceive.index') }}" class="nav-link {{ $active == 'drug_receive' ? 'active' : '' }} legitRipple"><i class="icon-database-check"></i><span>Receive</span></a></li>
                    @endpermission
                    @permission(['menu-drug-distribution'])
                        <li class="nav-item"><a href="{{ route('drugIn.index') }}" class="nav-link {{ $active == 'user' ? 'active' : '' }} legitRipple"><i class="icon-database-insert"></i><span>Distribution</span></a></li>
                    @endpermission
                    @permission('menu-drug-out')
                        <li class="nav-item"><a href="{{ route('drugOut.index') }}" class="nav-link {{ $active == 'drug_out' ? 'active' : '' }} "><i class="icon-database-export"></i><span>Check Out</span></a></li>
                    @endpermission
                 @endpermission
                 @permission(['menu-user','menu-role','menu-permission','menu-drug-receive'])
                 <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Report</div> <i class="icon-menu" title="Main"></i></li>
                    @permission(['menu-drug-receive'])
                        <li class="nav-item"><a href="{{ route('reportReceive.index') }}" class="nav-link {{ $active == 'report-receive' ? 'active' : '' }} legitRipple"><i class="icon-books"></i><span>Receive</span></a></li>
                    @endpermission
                    @permission(['menu-drug-receive'])
                        <li class="nav-item"><a href="{{ route('reportDistribution.index') }}" class="nav-link {{ $active == 'report-distribution' ? 'active' : '' }} legitRipple"><i class="icon-books"></i><span>Distribution</span></a></li>
                    @endpermission
                    @permission(['menu-drug-receive'])
                        <li class="nav-item"><a href="{{ route('reportOut.index') }}" class="nav-link {{ $active == 'report-out' ? 'active' : '' }} legitRipple"><i class="icon-books"></i><span>Out</span></a></li>
                    @endpermission
                    @permission(['menu-drug-receive'])
                        <li class="nav-item"><a href="{{ route('reportLPLPO.index') }}" class="nav-link {{ $active == 'lplpo' ? 'active' : '' }} legitRipple"><i class="icon-books"></i><span>LPLPO</span></a></li>
                    @endpermission
                    @permission(['menu-drug-receive'])
                        <li class="nav-item"><a href="{{ route('reportLPLPO.index') }}" class="nav-link {{ $active == 'lplpo' ? 'active' : '' }} legitRipple"><i class="icon-books"></i><span>LPLPO</span></a></li>
                    @endpermission
                 @endpermission
             </ul>
         </div>
     </div>
 </div>