<div class="navbar navbar-expand-lg navbar-light fixed-bottom">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler">
            &copy; 2021. <a href="route('home')">Niam Alfiyan Ahsan</a>
        </button>
    </div>

    <div class="navbar-collapse collapse text-center" id="navbar-footer">
        <span class="navbar-text text-center">
            &copy; 2021. <a href="route('home')">Niam Alfiyan Ahsan</a>
        </span>
    </div>
</div>