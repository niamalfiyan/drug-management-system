<div class="navbar navbar-expand-md navbar-dark bg-indigo fixed-top">
    <div class="navbar-brand text-white pb-0 mb-0">
        <div class="d-inline-block pb-0 mb-0">
            <h6>{{ env('APP_SUB_TITLE')}}</h6>
        </div>
    </div>


    <div class="d-md-none">
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block legitRipple">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>

            
        </ul>

        <span class="badge bg-success-400 badge-pill mr-md-auto">Online</span>

        <ul class="navbar-nav">
            <!--li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle legitRipple" data-toggle="dropdown">
                    <img src="../../../../global_assets/images/lang/gb.png" class="img-flag mr-2" alt="">
                    English
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item english active"><img src="../../../../global_assets/images/lang/gb.png" class="img-flag" alt=""> English</a>
                    <a href="#" class="dropdown-item ukrainian"><img src="../../../../global_assets/images/lang/ua.png" class="img-flag" alt=""> Українська</a>
                    <a href="#" class="dropdown-item deutsch"><img src="../../../../global_assets/images/lang/de.png" class="img-flag" alt=""> Deutsch</a>
                    <a href="#" class="dropdown-item espana"><img src="../../../../global_assets/images/lang/es.png" class="img-flag" alt=""> España</a>
                    <a href="#" class="dropdown-item russian"><img src="../../../../global_assets/images/lang/ru.png" class="img-flag" alt=""> Русский</a>
                </div>
            </li-->

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle legitRipple" data-toggle="dropdown">
                    @if(auth::user()->photo)
                        <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="rounded-circle mr-2" alt="profile_photo" height="34">
                    @else
                        @if(strtoupper(auth::user()->sex) == 'LAKI')
                            <img src="{{ asset('images/male_avatar.png') }}" class="rounded-circle mr-2" alt="avatar_male" height="34">
                        @else
                            <img src="{{ asset('images/female_avatar.png') }}" class="rounded-circle mr-2" alt="avatar_female" height="34" >
                        @endif
                    @endif
                    <span>{{ ucwords(Auth::user()->name) }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{ route('accountSetting') }}" class="dropdown-item {{ $active == 'account_setting' ? 'active' : '' }}"><i class="icon-cog5"></i> Account settings</a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>

    
</div>