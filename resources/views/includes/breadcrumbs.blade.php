<div class="page-header page-header-light" style="margin-bottom: 0;">
    <div class="page-header-content">
        <div class="page-title" style="padding-top: 1em; ;padding-bottom: 1em; ">
            <h5 class="font-weight-bold">
                <i class="icon-grid5 mr-2"></i>
                {{ $title }}
            </h5>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-inline">
        <div class="breadcrumb">
            <a href="{{ route('home')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
            @foreach ($lists as $item)
                @if($item['name'] == $active)   <span class="breadcrumb-item active">{{ $item['name'] }} </span>  
                @else <a href="{{ $item['url'] }} " class="breadcrumb-item">{{ $item['name'] }} </a>
                @endif
            @endforeach
        </div>

        @isset($actions)
            <div class="header-elements">
                <div class="breadcrumb">
                    <div class="breadcrumb-elements-item dropdown p-0">
                        <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                            Action
                        </a>

                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach ($actions as $action)
                                <a href="{{ $action->url }}" class="dropdown-item"><i class="{{ $action->iconClass }}"></i> 	<span>{{ $action->name }}</span> </a>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>
</div>