<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }} col-xs-12">
	@if (isset($label))
		<label
			for="{{ $field }}" class="control-label {{ isset($label_col) ? $label_col : 'col-xs-2' }} text-semibold"
		>
			{{ $label }}
		</label>
	@endif
	<div class="{{ isset($form_col) ? $form_col : 'col-xs-10'}}">
		{!! 
			Form::file(
				$field,
				[
					'class' => 'file-styled'.(isset($class) ? $class : ''),
				] + (isset($attributes) ? $attributes : [])
			)
		!!}

		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if (isset($mandatory))
			<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>