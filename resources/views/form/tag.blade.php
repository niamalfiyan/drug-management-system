<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }} col-lg-12 {{ isset($div_class) ? $div_class : '' }}" id="{{ isset($id_div) ? $id_div : ''  }}">
	@if (isset($label))
		<label
			for="{{ $field }}" class="text-semibold control-label {{ isset($label_col) ? $label_col : 'col-xs-2' }}"
		>
			{{ $label }}
		</label>
	@endif
	<div class="control-input {{ $errors->has($field) ? 'has-error' : '' }} {{ isset($form_col) ? $form_col : ''}}">
		{!! 
			Form::text(
				$field,
				isset($default) ? $default : null,
				[
					'class' => 'tags-input' . (isset($class) ? $class : ''),
					'placeholder' => isset($placeholder) ? $placeholder : ''
				] + (isset($attributes) ? $attributes : [])
			)
		!!}

		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if (isset($mandatory))
			<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>




