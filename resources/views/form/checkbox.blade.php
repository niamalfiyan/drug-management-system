<div id="{{ $field }}_error" class="form-group {{ $errors->has($field) ? 'has-error' : '' }} col-lg-12">
	@if (isset($label))
	<label
		for="{{ $field }}" class="control-label text-semibold {{ isset($label_col) ? $label_col : 'col-xs-2' }}"
	>
		{{ $label }}
	</label>
	@endif
	<div class="{{ isset($style_checkbox) ? $style_checkbox : 'control-input' }} {{ isset($form_col) ? $form_col : 'col-xs-10' }} {{ $errors->has($field) ? 'has-error' : ''}}">
			<label>
				{!! Form::checkbox (
						$field, 
						isset($default) ? $default : '',
						isset($checked) ? $checked : false,
						[
							'class' => (isset($class) ? $class : 'form-control')
						] + (isset($attributes) ? $attributes : [])
					)  
				!!}
				{{ isset($placeholder) ? $placeholder : ''}}
			</label>
		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if (isset($mandatory))
			<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>