<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }} row {{ isset($div_class) ? $div_class : ''  }}" id="{{ $field }}Div">
	@if (isset($label))
		<label
			for="{{ $field }}" class="text-semibold control-label {{ isset($label_col) ? $label_col : 'col-lg-2' }}"
		>
			{{ $label }}
		</label>
	@endif
	<div class="{{ isset($form_col) ? $form_col : 'col-lg-10' }}">
		{!! 
		Form::select(
			$field,
			$options,
			isset($default) ? $default : 'telek',
			[
				'class' => 'form-control ' . (isset($class) ? $class : '')
			] + (isset($attributes) ? $attributes : [])
		) 
	!!}

		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if (isset($mandatory))
			<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
</div>