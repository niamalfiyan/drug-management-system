@if (isset($btn_text))
	<div id="{{ $field }}_error" class="form-group {{ $errors->has($field) ? 'has-error' : '' }} row">
		@if (isset($label))
			<label
				for="{{ $field }}" class="col-form-label {{ isset($label_col) ? $label_col : 'col-xs-2' }}"
			>
				{{ $label }}
			</label>
		@endif

		<div class="col-xs-10">
			<div id="{{ $field }}_error" class="input-group {{ $errors->has($field) ? 'has-error' : '' }}">
				{!! 
					Form::text(
						$field,
						isset($default) ? $default : '',
						[
							'class' => 'form-control ' . (isset($class) ? $class : ''),
							'placeholder' => isset($placeholder) ? $placeholder : '',
							'autocomplete' => 'off',
							'autofocus' => 'autofocus'
						] + (isset($attributes) ? $attributes : [])
					)
				!!}

				
				
				<div class="input-group-btn">
					<button class="btn btn-default" type="button" id="{{ $field }}Button">
						<span class="{{ isset($btn_text) ? $btn_text : 'glyphicon glyphicon-pencil' }}"></span>
					</button>
				</div>

				
			</div>
		</div>	
		@if (isset($help))
			<span class="help-block">{{ $help }}</span>
		@endif
		@if (isset($mandatory))
			<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
		@endif
		@if ($errors->has($field))
			<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
@else

<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }} row">
	@if (isset($label))
		<label
			for="{{ $field }}" class="col-form-label {{ isset($label_col) ? $label_col : 'col-xs-2' }}"
		>
			{{ $label }}
		</label>
	@endif

	<div class="control-input {{ $errors->has($field) ? 'has-error' : '' }} {{ isset($form_col) ? $form_col : ''}}">
		{!! 
			Form::text(
				$field,
				isset($default) ? $default : null,
				[
					'class' => 'form-control ' . (isset($class) ? $class : ''),
					'placeholder' => isset($placeholder) ? $placeholder : '',
					'autocomplete' => 'off',
					
				] + (isset($attributes) ? $attributes : [])
			)
		!!}

		@if (isset($help))
		<span class="help-block">{{ $help }}</span>
		@endif
		@if (isset($mandatory))
			<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
		@endif
		@if ($errors->has($field))
		<span class="help-block text-danger">{{ $errors->first($field) }}</span>
		@endif
	</div>
	@if (isset($label2))
		<label
			for="{{ $field }}" class="control-label {{ isset($label_col2) ? $label_col2 : ''}}"
		>
			{{ $label2 }}
		</label>
	@endif
</div>
@endif




