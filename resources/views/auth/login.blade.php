@extends('layouts.app',['active' => 'login'])

@section('page-content')
    <div class="content-wrapper">
        <div class="content d-flex justify-content-center align-items-center">
                {!!
                    Form::open([
                        'url'       => route('login'),
                        'method'    => 'post',
                        'class'     => 'login-form',
                        'id'        => 'form'
                    ])
                !!}
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Login to your account</h5>
                                <span class="d-block text-muted">Your credentials</span>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" class="form-control {{ $errors->has('nik') || $errors->has('email') ? 'border-danger' : '' }}" value="{{ old('nik') ?: old('email') }}" placeholder="NIK / E-mail" name="login" required autocomplete="off">
                                <div class="form-control-feedback">
                                    <i class="icon-user"></i>
                                </div>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <div class="input-group">
                                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
                                    <span class="input-group-append">
                                        <button class="btn btn-default legitRipple" type="button" onClick="showPassword()"><i class="icon-eye-blocked" id="iconEye"></i></button>
                                    </span>
                                    <div class="form-control-feedback form-control-feedback-lg">
                                        <i class="icon-lock2"></i>
                                    </div>
                                </div>
                                
                            </div>

                            @if ($errors->has('nik'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('nik') }}</strong>
                                </span>
                            @elseif ($errors->has('email'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @elseif (session()->has('flash_notification.message'))
                                <span class="help-block text-danger">
                                    <strong>{!! session()->get('flash_notification.message') !!}</strong>
                                </span>   
                            @endif
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
                            </div>

                        

                        </div>
                    </div>
                {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('page-js')
    <script>
        function showPassword()
        {
            var text_field  = document.getElementById("password");
            var icon        = document.getElementById("iconEye");
            
            if (text_field.type === "password") text_field.type = "text";
            else text_field.type = "password";
            
            if(icon.className=="icon-eye-blocked") icon.className = "icon-eye";
            else icon.className = "icon-eye-blocked";
        }
    </script>
@endsection
