@extends('layouts.app',['active' => 'drug'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Master Data'
            ],
            [
                'url'  => route('drug.index'),
                'name' => 'Obat'
            ],
            [
                'url'  => '#',
                'name' => 'Create'
            ]
        ],
        'title'     => 'Obat',
        'active'    => 'Create'
    ])
@endsection

@section('page-content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                {!!
                    Form::open([
                        'role'      => 'form',
                        'url'       => route('drug.store'),
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'id'        => 'form'
                    ])
                !!}
                    <fieldset class="mb-3">
                        @include('form.text', [
                            'field'         => 'code',
                            'label'         => 'Kode',
                            'placeholder'   => 'Input code here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'code'
                            ]
                        ])
                        @include('form.text', [
                            'field'         => 'name',
                            'label'         => 'Nama',
                            'placeholder'   => 'Input name here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'name'
                            ]
                        ])


                        @include('form.select', [
                                'field'         => 'type',
                                'label'         => 'Jenis',
                                'options'       => [
                                    ''            => '-- Pilih Jenis --',
                                    'generik'     => 'Generik',
                                    'non generik' => 'Non Generik',
                                    'bmhp'        => 'Bmhp',
                                ],
                                'class'         => 'select-search',
                                'attributes'    => [
                                    'id'        => 'select_type'
                                ]
                            ])

                        @include('form.text', [
                            'field'         => 'price',
                            'label'         => 'Harga',
                            'placeholder'   => 'Input price here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'price'
                            ]
                        ])

                    </fieldset>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Submit  <i class="icon-floppy-disk ml-2"></i></button>
                    </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{ mix('js/drug.js') }}"></script>
@endsection