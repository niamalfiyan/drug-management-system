@extends('layouts.app',['active' => 'permission'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Master Data'
            ],
            [
                'url'  => route('drug.index'),
                'name' => 'Obat'
            ],
            [
                'url'  => '#',
                'name' => 'Edit'
            ]
        ],
        'title'     => 'Obat',
        'active'    => 'Edit'
    ])
@endsection

@section('page-content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                {!!
                    Form::open([
                        'role'      => 'form',
                        'url'       =>  route('drug.update',$drug->id),
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'id'        => 'form'
                    ])
                !!}
                    <fieldset class="mb-3">
                        
                        @include('form.text', [
                            'field'         => 'code',
                            'label'         => 'Kode',
                            'default'       => $drug->code,
                            'placeholder'   => 'Input code here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'code'
                            ]
                        ])
                        @include('form.text', [
                            'field'         => 'name',
                            'label'         => 'Nama',
                            'default'       => $drug->name,
                            'placeholder'   => 'Input name here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'name'
                            ]
                        ])

                        @include('form.text', [
                            'field'         => 'type',
                            'label'         => 'Jenis',
                            'default'       => $drug->type,
                            'placeholder'   => 'Input Type here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'name'
                            ]
                        ])

                        @include('form.text', [
                            'field'         => 'price',
                            'label'         => 'Harga',
                            'default'       => $drug->price,
                            'placeholder'   => 'Input price here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'price'
                            ]
                        ])

                    </fieldset>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Submit  <i class="icon-floppy-disk ml-2"></i></button>
                    </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{ mix('js/drug.js') }}"></script>
@endsection