@extends('layouts.app',['active' => 'drug'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Master'
            ],
            [
                'url'  => route('drug.index'),
                'name' => 'Obat'
            ]
        ],
        'title'     => 'Obat',
        'active'    => 'Obat',
        'actions'   => $breadcrumbActions
    ])
@endsection


@section('page-content')
    <div class="content">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="dtable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>Kode Obat</th>
                            <th>Nama</th>
                            <th>Jenis</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                </table>
            </div>
           
        </div>
    </div>
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/drug.js') }}"></script>
@endsection
