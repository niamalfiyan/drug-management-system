@extends('layouts.app',['active' => 'report-receive'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Account Management'
            ],
            [
                'url'  => route('reportReceive.index'),
                'name' => 'Permission'
            ],
            [
                'url'  => '#',
                'name' => 'Edit'
            ]
        ],
        'title'     => 'Report Receive',
        'active'    => 'Edit'
    ])
@endsection

@section('page-content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                {!!
                    Form::open([
                        'role'      => 'form',
                        'url'       =>  route('reportReceive.update',$data_receive->id),
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'id'        => 'form'
                    ])
                !!}
                <div class="row">
                            <div class="col-md-6">
                            <fieldset class="mb-3">
                    @include('form.select', [
                                    'field'         => 'asal',
                                    'label'         => 'Asal',
                                    'options'       => [
                                        'apbd kota'     => '-- APBD Kota --',
                                        'apbd provinsi' => '-- APBD Provinsi --',
                                        'mandiri'       => '-- Pembelian Mandiri --',
                                        'dak'           => '-- DAK --',
                                    ],
                                    'default'       => $data_receive->asal,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_asal'
                                    ]
                                ])

                                @include('form.text', [
                                    'field'         => 'qty_receive',
                                    'label'         => 'Qty',
                                    'default'       => $data_receive->qty_receive,
                                    'placeholder'   => 'Input Qty',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-lg-2',
                                    'form_col'      => 'col-lg-10',
                                    'attributes'    => [
                                        'id'        => 'qty_receive '
                                    ]
                                ])

        
                                @include('form.textarea', [
                                    'field'         => 'note',
                                    'label'         => 'Note',
                                    'default'       => $data_receive->note,
                                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes'    => [
                                        'id'        => 'note',
                                        'rows'      => 1,
                                        'style'      => 'resize: none;'
                                    ]
                                ])
        
                                
        
                            </fieldset>
                            </div>
                            <div class="col-md-6">
                                    @include('form.text', [
                                            'field'         => 'price',
                                            'label'         => 'Harga',
                                            'default'       => $data_receive->price,
                                            'placeholder'   => 'Input Harga',
                                            'mandatory'     => '*Required',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'attributes'    => [
                                                'id'        => 'price '
                                            ]
                                        ])

                                    @include('form.select', [
                                            'field'         => 'satuan',
                                            'default'       => $data_receive->satuan,
                                            'label'         => 'Satuan',
                                            'options'       => [
                                                'tablet' => '-- Tablet --',
                                                'sirup'  => '-- Sirup --',
                                                'kaplet' => '-- Kaplet --',
                                                'suppo'  => '-- Suppo --',
                                                'vial'   => '-- Vial --',
                                                'ampul'  => '-- Ampul --',
                                                'kapsul' => '-- Kapsul --',
                                                'pot'    => '-- Pot --',
                                                'tube'   => '-- Tube --',
                                                'kotak'  => '-- Kotak --',
                                                'sachet' => '-- Sachet --',
                                                'lembar' => '-- Lembar --',
                                                'buah'   => '-- Buah --',
                                                'box'    => '-- Box --',
                                                'strip'  => '-- Strip --',
                                                'pasang' => '-- Pasang --',
                                                'rol'    => '-- Rol --',
                                            ],
                                            'class'         => 'select-search',
                                            'attributes'    => [
                                                'id'        => 'select_satuan'
                                            ]
                                        ])

                                        @include('form.text', [
                                            'field'         => 'expired_date',
                                            'label'         => 'Expired',
                                            'default'       => $data_receive->expired_date,
                                            'placeholder'   => 'Input Expired',
                                            'mandatory'     => '*Required',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'attributes'    => [
                                                'id'        => 'expired '
                                            ]
                                        ])
                            </div>
                            </div>

                    </fieldset>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Submit  <i class="icon-floppy-disk ml-2"></i></button>
                    </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{ mix('js/report_receive.js') }}"></script>
@endsection