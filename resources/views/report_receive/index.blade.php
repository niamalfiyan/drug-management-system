@extends('layouts.app',['active' => 'report-receive'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Report'
            ],
            [
                'url'  => route('reportReceive.index'),
                'name' => 'Receive'
            ]
        ],
        'title'     => 'Receive',
        'active'    => 'report-receive',
    ])
@endsection


@section('page-content')
    <div class="content">
        <div class="card">
        <div class="card-body">
            {!!
				Form::open(array(
					'class' 	=> 'heading-form',
					'role' 		=> 'form',
					'url' 		=> route('reportReceive.export'),
					'method' 	=> 'get',
					'target' 	=> '_blank'		
				))
			!!}
            <div class="row">
				<div class="col-md-6 col-md-6 col-sm-12">
					@include('form.date', [
						'field' 		=> 'start_date',
						'label' 		=> 'Tanggal Mulai (00:00)',
						'label_col' 	=> 'col-md-4 col-lg-4 col-sm-12',
						'form_col' 		=> 'col-md-8 col-lg-8 col-sm-12',
						'default'		=> \Carbon\Carbon::now()->subDays(30)->format('d/m/Y'),
						'placeholder' 	=> 'dd/mm/yyyy',
						'class' 		=> 'daterange-single',
						'attributes'	=> [
							'id' 			=> 'start_date',
							'autocomplete' 	=> 'off',
							'readonly' 		=> 'readonly'
						]
					])
				</div>

				<div class="col-md-6 col-md-6 col-sm-12">
					@include('form.date', [
						'field' 		=> 'end_date',
						'label' 		=> 'Tanggal Akhir (23:59)',
						'label_col' 	=> 'col-md-4 col-lg-4 col-sm-12',
						'form_col' 		=> 'col-md-8 col-lg-8 col-sm-12',
						'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
						'placeholder' 	=> 'dd/mm/yyyy',
						'class' 		=> 'daterange-single',
						'attributes' 	=> [
							'id' 			=> 'end_date',
							'readonly' 		=> 'readonly',
							'autocomplete' 	=> 'off'
						]
					])
				</div>
			</div>

			<button type="submit" class="btn btn-receiveline alpha-primary border-primary text-primary-800 rounded-round legitRipple" >Export Report Receive<i class="icon-paperplane ml-2"></i></i></button>
			{!! Form::close() !!}

            <div class="table-responsive">
                <table class="table table-striped table-hover" id="dtable">
                    <thead>
                        <tr>
                            <th>No</th>
							<th>Id</th>
							<th>Tanggal Receive</th>
                            <th>Kode Obat</th>
                            <th>Nama Obat</th>
							<th>Jenis</th>
							<th>Satuan</th>
                            <th>Qty</th>
							<th>Tanggal Expired</th>
							<th>PIC</th>
                        </tr>
                    </thead>
                </table>
            </div>
                </div>
        </div>
    </div>
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/report_receive.js') }}"></script>
@endsection
