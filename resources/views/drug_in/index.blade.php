@extends('layouts.app',['active' => 'drug_in'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Transaction'
            ],
            [
                'url'  => route('drugIn.index'),
                'name' => 'In'
            ],
            [
                'url'  => '#',
                'name' => 'Create'
            ]
        ],
        'title'     => 'In',
        'active'    => 'Create'
    ])
@endsection

@section('page-content')
    <div class="content">
                <div class="card">
                    <div class="card-body">
                        {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('drugIn.create'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form'
                            ])
                        !!}
                        @include('form.select', [
                                    'field'         => 'locator_from',
                                    'label'         => 'Asal Distribusi',
                                    'options'       => [
                                        '' => '-- Pilih Distribusi --',
                                    ]+$from_locators,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_locator_id'
                                    ]
                                ])

                        @include('form.select', [
                                    'field'         => 'locator_id',
                                    'label'         => 'Distribusikan Ke',
                                    'options'       => [
                                        '' => '-- Pilih Distribusi --',
                                    ]+$locators,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_locator_id'
                                    ]
                                ])
                        <div class="row">
                            <div class="col-md-6">
                            <fieldset class="mb-3">
                            
                            @include('form.select', [
                                    'field'         => 'name',
                                    'label'         => 'Nama Obat',
                                    'options'       => [
                                        '' => '-- Pilih Nama Obat --',
                                    ]+$drugs,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_obat'
                                    ]
                                ])

                                @include('form.text', [
                                    'field'         => 'qty_in',
                                    'label'         => 'Qty',
                                    'placeholder'   => 'Input Qty',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-lg-2',
                                    'form_col'      => 'col-lg-10',
                                    'attributes'    => [
                                        'id'        => 'qty_in '
                                    ]
                                ])
        
                                
        
                            </fieldset>
                            </div>
                            <div class="col-md-6">


                                    @include('form.date', [
                                            'field' 		=> 'expired_date',
                                            'label' 		=> 'Tanggal Expired',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'mandatory'     => '*Required',
                                            'placeholder' 	=> 'dd/mm/yyyy',
                                            'class' 		=> 'daterange-single',
                                            'attributes'	=> [
                                                'id' 			=> 'created_at',
                                                'autocomplete' 	=> 'off',
                                                'readonly' 		=> 'readonly'
                                            ]
                                        ])
                                    
                                @include('form.textarea', [
                                    'field'         => 'note',
                                    'label'         => 'Note',
                                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes'    => [
                                        'id'        => 'note',
                                        'rows'      => 3,
                                        'style'      => 'resize: none;'
                                    ]
                                ])
                            </div>
                            </div>
        
                            {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">ADD<i class="icon-cart-add ml-2"></i></button>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
                <div class="card">
                <div class="card-body">
                    {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('drugIn.store'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form_store'
                            ])
                    !!}
                    <div class="table-responsive">
                        <div class="table-responsive">
                        <fieldset class="mb-3">
                            <table class="table datatable-basic table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Obat</th>
                                        <th>Nama Obat</th>
                                        <th>Qty</th>
                                        <th>Expired Date</th>
                                        <th>Note</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_drug_receive"></tbody>
                            </table>
                            </fieldset>
                        </div>
                    </div>
                    {!! Form::hidden('list_receive','[]' , array('id' => 'list_receive')) !!}
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Save<i class="icon-floppy-disk ml-2"></i></button>
                    </div>
                    {!! Form::close() !!}
                </div>
                </div>
            </div>
        </div>
    {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
@endsection

@section('page-js')
@include('drug_receive._table')
	<script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
    <script src="{{ mix('js/drug_in.js') }}"></script>
@endsection
