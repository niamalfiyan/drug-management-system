@extends('layouts.app',['active' => 'doctor'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Master Data'
            ],
            [
                'url'  => route('doctor.index'),
                'name' => 'Dokter'
            ]
        ],
        'title'     => 'Dokter',
        'active'    => 'Dokter',
        'actions'   => $breadcrumbActions
    ])
@endsection


@section('page-content')
    <div class="content">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="dtable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                </table>
            </div>
           
        </div>
    </div>
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/doctor.js') }}"></script>
@endsection
