<script type="x-tmpl-mustache" id="permission_table">
	{% #item %}
		<tr>
			<td style="text-align:center;">{% no %}</td>
			<td>
				{% name %}
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-icon-anim btn-delete-item"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
</script>