@extends('layouts.app',['active' => 'role'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Account Management'
            ],
            [
                'url'  => route('role.index'),
                'name' => 'Role'
            ],
            [
                'url'  => '#',
                'name' => 'Create'
            ]
        ],
        'title'     => 'Role',
        'active'    => 'Create'
    ])
@endsection

@section('page-content')
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('role.store'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form'
                            ])
                        !!}
                            <fieldset class="mb-3">
                                @include('form.text', [
                                    'field'         => 'name',
                                    'label'         => 'Name',
                                    'placeholder'   => 'Input name here',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-lg-2',
                                    'form_col'      => 'col-lg-10',
                                    'attributes'    => [
                                        'id'        => 'name'
                                    ]
                                ])
        
                                @include('form.textarea', [
                                    'field'         => 'description',
                                    'label'         => 'Description',
                                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes'    => [
                                        'id'        => 'description',
                                        'rows'      => 5,
                                        'style'      => 'resize: none;'
                                    ]
                                ])
        
                                @include('form.select', [
                                    'field'         => 'permission',
                                    'label'         => 'Permission',
                                    'options'       => [
                                        '' => '-- Select Permission --',
                                    ]+$permissions,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_permission'
                                    ]
                                ])
        
                            </fieldset>
        
                            {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Submit  <i class="icon-floppy-disk ml-2"></i></button>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table class="table datatable-basic table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Permission</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="role_permission"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
@endsection

@section('page-modal')
	@include('role._table')
@endsection

@section('page-js')
<script src="{{ mix('js/role.js') }}"></script>
@endsection
