@extends('layouts.app',['active' => 'role'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Account Management'
            ],
            [
                'url'  => route('role.index'),
                'name' => 'Role'
            ]
        ],
        'title'     => 'Role',
        'active'    => 'Role',
        'actions'   => $breadcrumbActions
    ])
@endsection

@section('page-content')
    <div class="content">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="dtable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
    {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
<script src="{{ mix('js/role.js') }}"></script>
@endsection
