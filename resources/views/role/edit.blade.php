@extends('layouts.app',['active' => 'role'])


@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Account Management'
            ],
            [
                'url'  => route('role.index'),
                'name' => 'Role'
            ],
            [
                'url'  => '#',
                'name' => 'Edit'
            ]
        ],
        'title'     => 'Role',
        'active'    => 'Edit'
    ])
@endsection

@section('page-content')
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('role.update',$role->id),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form'
                            ])
                        !!}
                            <fieldset class="mb-3">
                                @include('form.text', [
                                    'field'         => 'name',
                                    'label'         => 'Name',
                                    'default'       => $role->name,
                                    'placeholder'   => 'Input name here',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-lg-2',
                                    'form_col'      => 'col-lg-10',
                                    'attributes'    => [
                                        'id'        => 'name'
                                    ]
                                ])
        
                                @include('form.textarea', [
                                    'field'         => 'description',
                                    'label'         => 'Description',
                                    'default'       => $role->description,
                                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes'    => [
                                        'id'        => 'description',
                                        'rows'      => 5,
                                        'style'      => 'resize: none;'
                                    ]
                                ])
        
                                @include('form.select', [
                                    'field'         => 'permission',
                                    'label'         => 'Permission',
                                    'options'       => [
                                        '' => '-- Select Permission --',
                                    ]+$permissions,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_permission'
                                    ]
                                ])
        
                            </fieldset>
        
                            {!! Form::hidden('role_id', $role->id, array('id' => 'role_id')) !!}
                            {!! Form::hidden('mappings', json_encode($role->permissions()->get()), array('id' => 'mappings')) !!}
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Submit  <i class="icon-floppy-disk ml-2"></i></button>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table class="table datatable-basic table-striped table-hover" id="permissionTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Permission</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}  
    {!! Form::hidden('url_data_permission', route('role.dataPermission',$role->id), array('id' => 'url_data_permission')) !!}         
@endsection

@section('page-modal')
	@include('role._table')
@endsection

@section('page-js')
<script src="{{ mix('js/role.js') }}"></script>
@endsection
