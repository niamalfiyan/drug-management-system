@extends('layouts.app',['active' => 'user'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Account Management'
            ],
            [
                'url'  => route('user.index'),
                'name' => 'User'
            ]
        ],
        'title'     => 'User',
        'active'    => 'User',
        'actions'   => $breadcrumbActions
    ])
@endsection

@section('page-content')
<div class="content">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="dtable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id</th>
                        <th>Department</th>
                        <th>Nik</th>
                        <th>Nama</th>
                        <th>Sex</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

{!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
{!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
{!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection


@section('page-js')
    <script src="{{ mix('js/user.js') }}"></script>
@endsection
