<div class="list-icons">
    <div class="dropdown">
        <a href="#" class="list-icons-item" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>

        <div class="dropdown-menu dropdown-menu-right">
            <a href="{!! $edit !!}" class="dropdown-item"><i class="icon-pencil6"></i> Edit</a>
            <a href="#" onclick="hapus('{!! $delete !!}')" class="dropdown-item"><i class="icon-trash"></i> Hapus</a>
            <a href="#" onclick="reset('{!! $reset !!}')" class="dropdown-item" ><i class="icon-reset"></i> Reset Sandi</a>
        </div>
    </div>
</div>
