<div class="list-icons">
    <div class="dropdown">
        <a href="#" class="list-icons-item" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>

        <div class="dropdown-menu dropdown-menu-right">
            <a href="{!! $detail !!}" class="dropdown-item"><i class="icon-search4"></i> Detail</a>
            
        </div>
    </div>
</div>