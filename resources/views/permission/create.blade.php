@extends('layouts.app',['active' => 'permission'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Account Management'
            ],
            [
                'url'  => route('permission.index'),
                'name' => 'Permission'
            ],
            [
                'url'  => '#',
                'name' => 'Create'
            ]
        ],
        'title'     => 'Permission',
        'active'    => 'Create'
    ])
@endsection

@section('page-content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                {!!
                    Form::open([
                        'role'      => 'form',
                        'url'       => route('permission.store'),
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'id'        => 'form'
                    ])
                !!}
                    <fieldset class="mb-3">
                        @include('form.text', [
                            'field'         => 'name',
                            'label'         => 'Name',
                            'placeholder'   => 'Input name here',
                            'mandatory'     => '*Required',
                            'label_col'     => 'col-lg-2',
                            'form_col'      => 'col-lg-10',
                            'attributes'    => [
                                'id'        => 'name'
                            ]
                        ])

                        @include('form.textarea', [
                            'field'         => 'description',
                            'label'         => 'Description',
                            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                            'attributes'    => [
                                'id'        => 'description',
                                'rows'      => 5,
                                'style'      => 'resize: none;'
                            ]
                        ])

                    </fieldset>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Submit  <i class="icon-floppy-disk ml-2"></i></button>
                    </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{ mix('js/permission.js') }}"></script>
@endsection