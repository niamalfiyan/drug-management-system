<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="{{ env('APP_DESC') }}">
        <meta name="author" content="ICT">
        <meta property="og:url" content="{{ env('APP_URL') }}">
        <meta property="og:description" content="{{ env('APP_DESC') }}">
        <meta property="og:title" content="{{ env('APP_NAME') }}">
        <meta property="og:type" content="website">
        <meta property="og:locale" content="en_ID">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Drug Management System</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/limitless.css') }}" rel="stylesheet" type="text/css">
        @yield('content-css')
    </head>

    <body class="{{ Auth::guest() ? 'login-cover' : 'system-cover navbar-top' }}">
        @include('includes.top_notif')
        @if(Auth::check())
            @include('includes.main_navbar')
        @endif

        <div class="page-content">
            @if(Auth::check())
                @include('includes.side_navbar')
            @endif

            <div class="content-wrapper">

                @if(Auth::check())
                    @yield('page-breadcrumbs')
                @endif
                
                @yield('page-content')
                @yield('page-modal')

                <script src="{{ mix('js/limitless.js') }}"></script>
                <script src="{{ mix('js/app.js') }}"></script>
                <script src="{{ mix('js/notification.js') }}"></script>
                
                @if(Auth::check())
                <script type="text/javascript">
                        let timestamp = {{ time() }}
                        function updateTime(){
                            time=new Date();
                            second=time.getSeconds();
                            minute=time.getMinutes();
                            hour=time.getHours();
                            if(second<10){
                                second='0'+second;
                            }
                            if(minute<10){
                                minute='0'+minute;
                            }
                            if(hour<10){
                                hour='0'+hour;
                            }
                    
                            let _month = parseInt(time.getMonth())+1;
                            if(_month < 10) _month = '0'+_month;
                            else _month;
                    
                            let _date = time.getDate();
                            if(_date < 10) _date = '0'+_date;
                            else _date;
                    
                            $('#time').html(time.getFullYear()+'-'+_month+'-'+_date+' '+hour+':'+minute+':'+second);
                            timestamp++;
                            
                        }
                        $(function(){
                            setInterval(updateTime, 1000);
                        });
                    
                    </script>

                    @include('includes.footer')
                @endif
            </div>
        </div>
        
        @yield('page-js')
    </body>
</html>
