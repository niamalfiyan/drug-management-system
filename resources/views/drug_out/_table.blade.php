<script type="x-tmpl-mustache" id="drug_receive_table">
	{% #item %}
		<tr id="panel_{% _idx %}">
			<td>
				{% no %}.
			</td>
			<th>
				{% code %}
			</th>
			<td>
				{% nama %}
			</td>
			<td>
				{% qty %}
			</td>
			<td>
				{% racikan %}
			</td>
			<td>
				{% dosis %}
			</td>
			<td>
				<button type="button" id="delete_{% _idx %}" data-id="{% _idx %}" class="btn btn-danger btn-icon-anim btn-delete-item"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
</script>