@extends('layouts.app',['active' => 'drug_out'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Transaction'
            ],
            [
                'url'  => route('drugOut.index'),
                'name' => 'Out'
            ],
            [
                'url'  => '#',
                'name' => 'Check Out'
            ]
        ],
        'title'     => 'Out',
        'active'    => 'drug_out'
    ])
@endsection

@section('page-content')
    <div class="content">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <p>Nama Pasien <b> {{$recipe->nama_pasien}}</b></p>
                                <p>Alamat <b>{{$recipe->alamat}}</b></p>
                                <p>Tanggal Lahir <b>{{$recipe->tanggal_lahir}}</b></p>
                                <p>Tanggal Resep <b>{{$recipe->tanggal_resep}}</b></p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <p>Jenis Kelamin <b>{{$recipe->jenis_kelamin}}</b></p>
                                <p>Diagnosis <b>{{$recipe->diagnosis}}</b></p>
                                <p>Berat Badan <b>{{$recipe->berat_badan}}</b></p>
                                <p>Tekanan Darah <b>{{$recipe->tekanan_darah}}</b></p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <p>Nama Dokter <b>{{$recipe->Doctor->name}}</b></p>
                                <p>Status Jaminan <b>{{$recipe->status_jaminan}}</b></p>
                                <p>Tempat Pemeriksaan <b>{{$recipe->tempat_pemeriksaan}}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('drugOut.create'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form'
                            ])
                        !!}

                        @include('form.select', [
                                    'field'         => 'locator_from',
                                    'label'         => 'Asal',
                                    'options'       => [
                                        '' => '-- Pilih Sumber --',
                                    ]+$from_locators,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_locator_id'
                                    ]
                                ])

                        <div class="row">
                            <div class="col-md-6">
                            <fieldset class="mb-3">
                            @include('form.select', [
                                    'field'         => 'name',
                                    'label'         => 'Nama Obat',
                                    'options'       => [
                                        '' => '-- Pilih Nama Obat --',
                                    ]+$drugs,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_obat'
                                    ]
                                ])

                                @include('form.text', [
                                    'field'         => 'receive_qty',
                                    'label'         => 'Qty',
                                    'placeholder'   => 'Input Qty',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-lg-2',
                                    'form_col'      => 'col-lg-10',
                                    'attributes'    => [
                                        'id'        => 'receive_qty '
                                    ]
                                ])
                                
        
                            </fieldset>
                            </div>
                            <div class="col-md-6">
                                    @include('form.text', [
                                            'field'         => 'dosis',
                                            'label'         => 'Dosis',
                                            'placeholder'   => 'Input Dosis',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'attributes'    => [
                                                'id'        => 'price '
                                            ]
                                        ])

                                    @include('form.select', [
                                            'field'         => 'racikan',
                                            'label'         => 'racikan',
                                            'options'       => [
                                                '1' => '-- 1 --',
                                                '2' => '-- 2 --',
                                                '3' => '-- 3 --',
                                                '4' => '-- 4 --',
                                                '5' => '-- 5 --',
                                            ],
                                            'class'         => 'select-search',
                                            'attributes'    => [
                                                'id'        => 'select_satuan'
                                            ]
                                        ])

                            </div>
                            </div>
        
                            {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
                            {!! Form::hidden('recipe_id',$recipe->id , array('id' => 'recipe_id')) !!}
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">ADD<i class="icon-cart-add ml-2"></i></button>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            <div class="card">
                <div class="card-body">
                    {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('drugOut.store'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form_store'
                            ])
                    !!}
                    <div class="table-responsive">
                        <div class="table-responsive">
                        <fieldset class="mb-3">
                            <table class="table datatable-basic table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Obat</th>
                                        <th>Nama Obat</th>
                                        <th>Qty</th>
                                        <th>Racikan</th>
                                        <th>Dosis</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_drug_receive"></tbody>
                            </table>
                            </fieldset>
                        </div>
                    </div>
                    {!! Form::hidden('recipe_id',$recipe->id , array('id' => 'recipe_id')) !!}
                    {!! Form::hidden('list_receive','[]' , array('id' => 'list_receive')) !!}
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Save<i class="icon-floppy-disk ml-2"></i></button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            </div>
        </div>
    {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
@endsection

@section('page-js')
@include('drug_out._table')
	<script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
    <script src="{{ mix('js/drug_out.js') }}"></script>
@endsection
