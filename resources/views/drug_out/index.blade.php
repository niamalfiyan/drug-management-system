@extends('layouts.app',['active' => 'drug_out'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Transaction'
            ],
            [
                'url'  => route('drugOut.index'),
                'name' => 'Out'
            ],
            [
                'url'  => '#',
                'name' => 'Create Resep'
            ]
        ],
        'title'     => 'Out',
        'active'    => 'drug_out'
    ])
@endsection

@section('page-content')
    <div class="content">
                <div class="card">
                    <div class="card-body">
                        {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('drugOut.createRecipe'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form'
                            ])
                        !!}
                        <div class="row">
                            <div class="col-md-6">
                            <fieldset class="mb-3">
                            @include('form.text', [
                                    'field'         => 'nama_pasien',
                                    'label'         => 'Nama Pasien',
                                    'placeholder'   => 'Input Nama Pasien',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-lg-2',
                                    'form_col'      => 'col-lg-10',
                                    'attributes'    => [
                                        'id'        => 'nama_pasien'
                                    ]
                                ])

                                @include('form.textarea', [
                                    'field'         => 'alamat',
                                    'label'         => 'Alamat',
                                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes'    => [
                                        'id'        => 'alamat',
                                        'rows'      => 3,
                                        'style'      => 'resize: none;'
                                    ]
                                ])

                                @include('form.date', [
                                            'field' 		=> 'tanggal_lahir',
                                            'label' 		=> 'Tanggal Lahir',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'mandatory'     => '*Required',
                                            'placeholder' 	=> 'dd/mm/yyyy',
                                            'class' 		=> 'daterange-single',
                                            'attributes'	=> [
                                                'id' 			=> 'tanggal_lahir',
                                                'autocomplete' 	=> 'off',
                                                'readonly' 		=> 'readonly'
                                            ]
                                        ])

                                @include('form.date', [
                                            'field' 		=> 'tanggal_resep',
                                            'label' 		=> 'Tanggal Resep',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'mandatory'     => '*Required',
                                            'placeholder' 	=> 'dd/mm/yyyy',
                                            'class' 		=> 'daterange-single',
                                            'attributes'	=> [
                                                'id' 			=> 'tanggal_resep',
                                                'autocomplete' 	=> 'off',
                                                'readonly' 		=> 'readonly'
                                            ]
                                        ])
                                
                                @include('form.select', [
                                    'field'         => 'jenis_kelamin',
                                    'label'         => 'Jenis Kelamin',
                                    'options'       => [
                                        'laki-laki' => '-- Laki - Laki --',
                                        'perempuan' => '-- Perempuan --',
                                    ],
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_jenis_kelamin'
                                    ]
                                ])
        
                                
        
                            </fieldset>
                            </div>
                            <div class="col-md-6">
                                    @include('form.textarea', [
                                        'field'         => 'diagnosis',
                                        'label'         => 'Diagnosis',
                                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                        'attributes'    => [
                                            'id'        => 'diagnosis',
                                            'rows'      => 3,
                                            'style'      => 'resize: none;'
                                        ]
                                    ])

                                    @include('form.text', [
                                        'field'         => 'berat_badan',
                                        'label'         => 'BB',
                                        'placeholder'   => 'Input BB',
                                        'label_col'     => 'col-lg-2',
                                        'form_col'      => 'col-lg-10',
                                        'attributes'    => [
                                            'id'        => 'berat_badan '
                                        ]
                                    ])

                                    @include('form.text', [
                                        'field'         => 'tekanan_darah',
                                        'label'         => 'Tekanan Darah',
                                        'placeholder'   => 'Input Tekanan Darah',
                                        'label_col'     => 'col-lg-2',
                                        'form_col'      => 'col-lg-10',
                                        'attributes'    => [
                                            'id'        => 'tekanan_darah '
                                        ]
                                    ])

                                    @include('form.select', [
                                            'field'         => 'doctor_id',
                                            'label'         => 'Nama Dokter',
                                            'options'       => [
                                                'laki-laki' => '-- Pilih Dokter --',
                                            ]+$doctors,
                                            'class'         => 'select-search',
                                            'attributes'    => [
                                                'id'        => 'select_nama_dokter'
                                            ]
                                        ])
                                    
                                    @include('form.select', [
                                            'field'         => 'status_jaminan',
                                            'label'         => 'Status Jaminan',
                                            'options'       => [
                                                'pbi' => '-- PBI --',
                                                'non pbi' => '-- Non PBI --',
                                                'gratis' => '-- Gratis --',
                                                'lain-lain' => '-- Lain-Lain --',
                                            ],
                                            'class'         => 'select-search',
                                            'attributes'    => [
                                                'id'        => 'select_status_jaminan'
                                            ]
                                        ])
                                    
                                    @include('form.text', [
                                        'field'         => 'tempat_pemeriksaan',
                                        'label'         => 'Tempat Pemeriksaan',
                                        'placeholder'   => 'Input Tempat Pemeriksaan',
                                        'label_col'     => 'col-lg-2',
                                        'form_col'      => 'col-lg-10',
                                        'attributes'    => [
                                            'id'        => 'tempat_pemeriksaan '
                                        ]
                                    ])
                            </div>
                            </div>
        
                            {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">NEXT<i class="icon-cart-add ml-2"></i></button>
                            </div>
                            {!! Form::close() !!}
                    </div>
             </div>
        </div>
    {!! Form::hidden('form_status', 'create_resep', array('id' => 'form_status')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
    <script src="{{ mix('js/drug_out.js') }}"></script>
@endsection
