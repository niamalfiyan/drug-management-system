<script type="x-tmpl-mustache" id="drug_receive_table">
	{% #item %}
		<tr>
			<td>
				{% no %}.
			</td>
			<th>
				{% code %}
			</th>
			<td>
				{% nama %}
			</td>
			<td>
				{% qty %}
			</td>
			<td>
				{% satuan %}
			</td>
			<td>
				{% harga %}
			</td>
			<td>
				{% total %}
			</td>
			<td>
				{% expired_date %}
			</td>
			<td>
				{% note %}
			</td>
			<td>
				<button type="button" id="delete_{% _idx %}" data-id="{% _idx %}" class="btn btn-danger btn-icon-anim btn-delete-item"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
</script>