@extends('layouts.app',['active' => 'drug_receive'])

@section('page-breadcrumbs')
    @include('includes.breadcrumbs',[
        'lists'     => [ 
            [
                'url'  => '#',
                'name' => 'Transaction'
            ],
            [
                'url'  => route('drugReceive.index'),
                'name' => 'Receive'
            ],
            [
                'url'  => '#',
                'name' => 'Create'
            ]
        ],
        'title'     => 'Receive',
        'active'    => 'Create'
    ])
@endsection

@section('page-content')
    <div class="content">
                <div class="card">
                    <div class="card-body">
                        {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('drugReceive.create'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form'
                            ])
                        !!}
                        <div class="row">
                            <div class="col-md-6">
                            <fieldset class="mb-3">

                            @include('form.select', [
                                    'field'         => 'asal',
                                    'label'         => 'Asal',
                                    'options'       => [
                                        'apbd kota'     => '-- APBD Kota --',
                                        'apbd provinsi' => '-- APBD Provinsi --',
                                        'mandiri'       => '-- Pembelian Mandiri --',
                                        'dak'           => '-- DAK --',
                                    ],
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_asal'
                                    ]
                                ])
                            
                            @include('form.select', [
                                    'field'         => 'name',
                                    'label'         => 'Nama Obat',
                                    'options'       => [
                                        '' => '-- Pilih Nama Obat --',
                                    ]+$drugs,
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_obat'
                                    ]
                                ])

                                @include('form.text', [
                                    'field'         => 'receive_qty',
                                    'label'         => 'Qty',
                                    'placeholder'   => 'Input Qty',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-lg-2',
                                    'form_col'      => 'col-lg-10',
                                    'attributes'    => [
                                        'id'        => 'receive_qty '
                                    ]
                                ])

        
                                @include('form.textarea', [
                                    'field'         => 'note',
                                    'label'         => 'Note',
                                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes'    => [
                                        'id'        => 'note',
                                        'rows'      => 3,
                                        'style'      => 'resize: none;'
                                    ]
                                ])
        
                                
        
                            </fieldset>
                            </div>
                            <div class="col-md-6">
                                    @include('form.text', [
                                            'field'         => 'price',
                                            'label'         => 'Harga',
                                            'placeholder'   => 'Input Harga',
                                            'mandatory'     => '*Required',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'attributes'    => [
                                                'id'        => 'price '
                                            ]
                                        ])

                                    @include('form.select', [
                                            'field'         => 'satuan',
                                            'label'         => 'Satuan',
                                            'options'       => [
                                                'tablet' => '-- Tablet --',
                                                'sirup'  => '-- Sirup --',
                                                'kaplet' => '-- Kaplet --',
                                                'suppo'  => '-- Suppo --',
                                                'vial'   => '-- Vial --',
                                                'ampul'  => '-- Ampul --',
                                                'kapsul' => '-- Kapsul --',
                                                'pot'    => '-- Pot --',
                                                'tube'   => '-- Tube --',
                                                'kotak'  => '-- Kotak --',
                                                'sachet' => '-- Sachet --',
                                                'lembar' => '-- Lembar --',
                                                'buah'   => '-- Buah --',
                                                'box'    => '-- Box --',
                                                'strip'  => '-- Strip --',
                                                'pasang' => '-- Pasang --',
                                                'rol'    => '-- Rol --',
                                            ],
                                            'class'         => 'select-search',
                                            'attributes'    => [
                                                'id'        => 'select_satuan'
                                            ]
                                        ])

                                    @include('form.date', [
                                            'field' 		=> 'expired',
                                            'label' 		=> 'Expired Date',
                                            'label_col'     => 'col-lg-2',
                                            'form_col'      => 'col-lg-10',
                                            'mandatory'     => '*Required',
                                            'placeholder' 	=> 'dd/mm/yyyy',
                                            'class' 		=> 'daterange-single',
                                            'attributes'	=> [
                                                'id' 			=> 'expired',
                                                'autocomplete' 	=> 'off',
                                                'readonly' 		=> 'readonly'
                                            ]
                                        ])
                            </div>
                            </div>
        
                            {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">ADD<i class="icon-cart-add ml-2"></i></button>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
                <div class="card">
                <div class="card-body">
                    {!!
                            Form::open([
                                'role'      => 'form',
                                'url'       => route('drugReceive.store'),
                                'method'    => 'post',
                                'class'     => 'form-horizontal',
                                'id'        => 'form_store'
                            ])
                    !!}
                    <div class="table-responsive">
                        <div class="table-responsive">
                        <fieldset class="mb-3">
                            <table class="table datatable-basic table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Obat</th>
                                        <th>Nama Obat</th>
                                        <th>Qty</th>
                                        <th>Satuan</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                        <th>Expired Date</th>
                                        <th>Note</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_drug_receive"></tbody>
                            </table>
                            </fieldset>
                        </div>
                    </div>
                    {!! Form::hidden('list_receive','[]' , array('id' => 'list_receive')) !!}
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary rounded-round legitRipple btn-block" aria-expanded="false">Save<i class="icon-floppy-disk ml-2"></i></button>
                    </div>
                    {!! Form::close() !!}
                </div>
                </div>
            </div>
        </div>
    {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
@endsection

@section('page-js')
@include('drug_receive._table')
	<script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
    <script src="{{ mix('js/drug_receive.js') }}"></script>
@endsection
